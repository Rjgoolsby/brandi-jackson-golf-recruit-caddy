<?php

namespace app\console\controllers;

use Yii;
use yii\console\Controller;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\helpers\Console;
use app\models\helpers\HtmlParser;
use app\models\helpers\CaddyParser;
use yii\helpers\Json;
use app\models\helpers\Html2Text;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use yii\helpers\Html;


class CronController extends Controller {
 
    // protected $adminEmail = ['brandi@brandijacksongolf.com','golfrecruitingadvantage@pyrotechsolutions.com'];//'brandi@brandijacksongolf.com'
    protected $adminEmail = 'brandi@brandijacksongolf.com';//'brandi@brandijacksongolf.com'

    public function actionTestEnv(){
        if(YII_ENV_DEV) {
            echo 'ENV_DEV';
        }elseif(!YII_ENV_DEV){
            echo 'ENV_PROD';
        }else{
            echo "ENV Undetermined";
        }
    }

    /* public function actionTestParams(){
        $this->stdout(VarDumper::dumpAsString(Yii::$app->params['adminEmail'],10,false) . PHP_EOL, Console::FG_GREEN);
    } */

    protected function getFileTree($path){
        $iterator = new \RecursiveDirectoryIterator($path);
        $filter = new RecursiveDotFilterIterator($iterator);
		$files = new \RecursiveIteratorIterator($filter);
		$tree = array();
		foreach ($files as $file) {
			if (!$file->isDir() && in_array($file->getExtension(), ['html','htm'])){
                $name = $file->getBasename('.htm');
                preg_match('#\((.*?)\)#', $name, $match);
                list($gender) = explode('(', $name);
				$pages[] = [
                    'path'=>$file->getPathname(),
                    'name'=>$name,
                    'level'=>$match[1],
                    'gender'=> $gender,
                ];
			}
		}
		return (empty($pages))?[]:$pages;
	} 

    public function actionSetSchoolJson(){
        $list = $this->getFileTree(\Yii::getAlias('@app/data/schools'));
        foreach ($list as $key => $file) { 
            $files[] = [
                'path' => $file['path'],
                'gender' => ($file['gender'] == 'Mens')?10:20,
                'level'=> $file['level'],
            ];
        } 
        $model = new CaddyParser;
        $array = $model->updateSchools($files,false); 
        $schoolsJsonFile = \Yii::getAlias('@app/data/schools/schools.json');
        file_put_contents($schoolsJsonFile, Json::encode($array,JSON_NUMERIC_CHECK | JSON_PRESERVE_ZERO_FRACTION));
        $this->stdout(VarDumper::dumpAsString($array,10,false) . PHP_EOL, Console::FG_GREEN);
    } 

    public function actionSetSchools(){
        $model = new CaddyParser;
        $schoolsJsonFile = \Yii::getAlias('@app/data/schools/schools.json');
        $arraySchools = Json::decode(file_get_contents($schoolsJsonFile));
        // $this->stdout(VarDumper::dumpAsString($arraySchools,10,false) . PHP_EOL, Console::FG_GREEN);
        $response = $model->setSchools($arraySchools);
        $this->stdout(VarDumper::dumpAsString($response,10,false) . PHP_EOL, Console::FG_GREEN);
    }

    public function actionProcessUpdates(){

        $startTime = (int) \Yii::$app->formatter->asTimestamp('-1 day');
        $serviceAccount = ServiceAccount::fromJsonFile( dirname(__FILE__,3) .'/config/keys/google-service-account.json');  
        $firebase = (new Factory)
            ->withDisabledAutoDiscovery()
            ->withServiceAccount($serviceAccount)
            ->create();
        $database = $firebase->getDatabase();
        // $this->stdout(VarDumper::dumpAsString($startTime,10,false) . PHP_EOL, Console::FG_GREEN);
        $snapshot = $database->getReference('transactions')->orderByChild('timestamp')->startAt($startTime)->getSnapshot()->getValue();
        $transactions = ArrayHelper::index($snapshot, null, 'name'); 

        $records = [];
        $html = [];
 
        foreach ($transactions as $username => $records) {
            $lines = []; 
            $header = Html::tag('h1', $username . ' Updates');
            $dataset = ArrayHelper::index($records, null, 'school');
            $html[] = $header; 
            foreach ($dataset as $schoolName => $record) { 
                $subHeader = Html::tag('h2', $schoolName . ' Updates');
                $html[] = $subHeader;
                foreach ($record as $key => $update) {
                    $lines[] = $this->processUpdateLine($update);
                }   
                $updates = Html::ul($lines,['encode'=>false]);  
                $html[] =  $updates . '</br>';
            } 
        } 
        if(empty($html)){
            $messageBody = Html::tag('h1','Nothing To Read.');
            $messageBody .= Html::tag('h1','No Updates Available');
        }else{
            $messageBody = implode(PHP_EOL, $html);
        }
        $header = Html::tag('h1','Daily Recruit Caddy Updates');
        $body = Html::tag('div',$messageBody);
        $html2text = new Html2Text($body);
        $textHtml = $html2text->getText();

        // echo $body;
        // $this->stdout(VarDumper::dumpAsString($body,10,false) . PHP_EOL, Console::FG_GREEN);   
        
        $preparedEmail1 = Yii::$app->mailer->compose()
            ->setFrom('no-reply@pyrotechsolutions.com')
            ->setSubject('Recruit Caddy Daily Updates')
            ->setTo($this->adminEmail)
            ->setTextBody($textHtml)
            ->setHtmlBody($body)
            ->send(); 
        $preparedEmail2 = Yii::$app->mailer->compose()
            ->setFrom('no-reply@pyrotechsolutions.com')
            ->setSubject('Recruit Caddy Daily Updates')
            ->setTo('golfrecruitingadvantage@pyrotechsolutions.com')
            ->setTextBody($textHtml)
            ->setHtmlBody($body)
            ->send();  
        $this->stdout(VarDumper::dumpAsString([$preparedEmail1,$preparedEmail2],10,false) . PHP_EOL, Console::FG_GREEN);    
    } 

    protected function processUpdateLine($update){
        $type = ArrayHelper::getValue($update,'type');
        if(!empty($type)){
            if($type == 'updateVal'){
                $line = $this->buildUpdateVal($update);
            }else if($type == 'nextSteps'){
                $data = $type = ArrayHelper::getValue($update,'data','');
                $line = 'Updated Next Steps with this message: '. $data;
            }else if($type == 'likes/dislikes'){
                $likes = $type = ArrayHelper::getValue($update,'data.likes','');
                $dislikes = $type = ArrayHelper::getValue($update,'data.dislikes',''); 
                $line = 'Updated Likes to: '. $likes . '<br> Updated Dislikes to: '. $dislikes;
            }else if($type == 'addedSchool'){
                $line = $type = ArrayHelper::getValue($update,'data',''); 
            } else if($type == 'deleteSchool'){
                $line = $type = ArrayHelper::getValue($update,'data',''); 
            }
        }
        return (empty($line))? '' : $line;
    }

    protected function buildUpdateVal($update){
        $line = '';
        $data = $type = ArrayHelper::getValue($update,'data',[]); 
        foreach ($data as $key => $value) { 
            $status = ($value) ? 'true' : 'false';
            $line = 'Updated ' . $this->getValKey($key) . ' to ' . $status;
        } 
        return $line;
    }

    protected function getValKey($key){
        switch ($key) {
            case 'eval_status':
                $title = 'Evaluation Status';
                break;
            case 'player_rating':
                $title = 'Player School Rating';
                break;
            case 'intro_email_sent':
                $title = 'Intro Email Sent?';
                break;
            case 'intro_call_made':
                $title = 'Intro Call Made?';
                break;
            case 'follow_email_sent':
                $title = 'Follow Up Email Sent?';
                break;
            case 'follow_call_made':
                $title = 'Follow Up Call Made?';
                break;
            case 'visit':
                $title = 'Vist Taken?';
                break; 
            case 'offer_received':
                $title = 'Offer Received?';
                break; 
            default:
                $title='';
                break;
        }
        return $title;
    }
    /*public function actionSendTestEmail($message){
        Yii::$app->mailer->compose()
            ->setTo('Rgoolsby@firenet.gov')
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setSubject('Testing Console Mailing System')
            ->setTextBody($message)
            ->send();
    }

    public function actionBackupDb(){
        $backTime = -120;
        for ($i = $backTime; $i < 0 ; $i++) {
            $start = $i.' Days';
            $end = ( $i+1 === 0 )? 'now' :( $i+1).' Days';
            echo $start .'<->'. $end . PHP_EOL;
            $model = new Dashboard([
                'username'=>'goolsby_wws',
                'discBegin' => $i.' Days',
                'discEnd' => ($i+1).' Days',
            ]);

            $model->updateDbFeed();
        }

    }*/



}


class RecursiveDotFilterIterator extends  \RecursiveFilterIterator
{
	public function accept()
	{
		return '.' !== substr($this->current()->getFilename(), 0, 1);
	}
}
