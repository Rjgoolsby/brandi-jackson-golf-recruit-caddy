<?php
// set it to false when in production
defined('YII_DEBUG') or define('YII_DEBUG', true);

require_once(__DIR__ . '/vendor/autoload.php');
require_once(__DIR__ . '/vendor/yiisoft/yii2/Yii.php');
require_once(__DIR__ . '/config/bootstrap.php');

$config = require_once(__DIR__ . '/config/yii.php');

new yii\web\Application($config);