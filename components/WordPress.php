<?php

namespace common\components;

use Yii;
use yii\base\Component;
use yii\data\ArrayDataProvider;


class WordPress extends Component{
    public $homeUrl;
    public $userClass = 'common\models\User';
    protected $_userId;
    protected $_user;

    public function init(){
        $userID = get_current_user_id();
        if($userID == 0){
            return false;
            return Yii::$app->getResponse()->redirect($this->homeUrl.'/wp-login.php');
        }else{
            $this->_userId = $userID;
        }
        parent::init();
    }

    public function getIsGuest(){
        return $this->_userId == 0;
    }
    public function getId(){
        return $this->_userId;
    }
    public function logout(){
        return Yii::$app->getResponse()->redirect($this->homeUrl.'/wp-login.php?action=logout');
    }

    protected $_adminEmail;
    public function setAdminEmail($email = null){
        $this->_adminEmail = ($email == null)?get_option( 'admin_email' ):$email;
    }

    public function getAdminEmail(){
        if(!isset($this->_adminEmail)){
            $this->setAdminEmail();
        }
        // Yii::trace($this->_adminEmail,'dev');
        return $this->_adminEmail;
    }
/*
Yii::$app->wp->user->roles
users
ID
user_login
user_pass
user_nicename
user_email
user_url
user_registered
display_name
user_meta
first_name
last_name
nickname
description
wp_capabilities (array)
admin_color (Theme of your admin page. Default is fresh.)
closedpostboxes_page
primary_blog
rich_editing
source_domain
*/
    protected function setUser(){
        $this->_user = get_userdata($this->id);
    }
    public function getUser(){
        if($this->_user === null){
            $this->setUser();
        }
        return $this->_user;
        // return $this->userClass::findOne(['ID'=>$this->id]);
        // return \common\models\User::findOne(['ID'=>$this->id]);
        $user = Yii::createObject($this->userClass);
        return $user::findIdentity($this->id);
    }

    public function getAllUsersDataProvider(){
        return new ArrayDataProvider([
            'allModels' => $this->allUsers,
            'sort' => [
                'attributes' => ['id', 'username', 'email'],
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
    }
    protected $_allUsers;
    public function setAllUsers(){
        $users = get_users([]);
        $array = array();
        foreach($users as $user_id){
            $array[] = get_userdata($user_id->ID);
        }
        $this->_allUsers = $array;
    }
    public function getAllUsers(){
        if($this->_allUsers === null){
            $this->setAllUsers();
        }
        return $this->_allUsers;
    }

/*


    $listData=ArrayHelper::map($countries,'code','name');

public function isLogg
    $user_id = get_current_user_id();
    if ($user_id == 0) {
        echo 'You are currently not logged in.';
    } else {
        echo 'You are logged in as user '.$user_id.'.';
    }*/
}
