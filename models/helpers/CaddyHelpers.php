<?php
namespace app\models\helpers;

use Yii;
use yii\base\Model;
// use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\helpers\Html;
use kartik\helpers\Enum;

// Project models
use app\models\caddy\RecruitCaddy;
use app\models\caddy\RecruitCaddySearch;
use app\models\players\RecruitCaddyPlayers;
use app\models\schools\RecruitCaddySchools;
use app\models\players\RecruitCaddyPlayersSearch;
use app\models\schools\RecruitCaddySchoolsSearch;

class CaddyHelpers extends Model{

    public static function getSubscriptions(){
        return [
            10 => 'Recruit Basic',
            20 => 'Elite Premium',
        ];
    }

    public static function getEvalList(){
        return [
            10=>'Realistic',
            20=>'Fallback',
            30=>'Dream'
        ];
    }

    public static function getSchoolRatingList(){
         return [
            1 =>'1 Star',
            2 =>'2 Star',
            3 =>'3 Star',
            4 =>'4 Star',
            5 =>'5 Star',
        ];
    }

/*    public static function getSchoolLevels(){
        self::getSchoolList();

    }
    */
    public static function getSchoolList(){
        $droptions = RecruitCaddySchools::find()->asArray()->all();
        return $droptions;
        return ArrayHelper::map($droptions, 'id', 'name');
    }

    public static function createDropdown($array,$selected = [], $prompt = ''){
        // $keys = ArrayHelper::getColumn ( $array, 'id');
        // \Yii::trace($array,'dev');
        $html = [];
        if(!Enum::isEmpty($prompt)){
            $html[] =  Html::tag('option',$prompt,['disabled'=>true,'selected'=>true]);
        }

        foreach ($array as $value) {
            if(is_string($selected)){
                $isSelected = (!Enum::isEmpty($selected) && (int)$value['id'] === (int)$selected)?true:false;
            }else{
                $isSelected = (!Enum::isEmpty($selected) && in_array($value['id'], $selected))?true:false;
            }
            $html[] = Html::tag('option',$value['name'],['value'=>$value['id'], 'selected'=>$isSelected?true:false]);
        }
        return implode(PHP_EOL, $html);
    }


    public static function getActionColumn($exclude = []){
		$btns = ['view','update','delete'];
		$template = '';
		foreach ($exclude as  $value) {
			// Yii::trace($value,'dev');
			if(in_array($value, $btns)){
				$key = array_search($value, $btns);
				unset($btns[$key]);
			}
		}
		foreach ($btns as $key => $btn) {
			$template.='{'.$btn.'}</br>';
		}

    	return [
            'class' => 'yii\grid\ActionColumn',
            'headerOptions' => ['width' => '80'],
            'template'=> $template,//'{view}</br>{update}</br>{delete}',
            'buttons' => [
                //delete button
                'delete' =>  function ($url, $model, $key) {
                    $options = [
                        'title' => Yii::t('yii', 'Delete'),
                        'aria-label' => Yii::t('yii', 'Delete'),
                        'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                        'data-method' => 'post',
                        'data-pjax' => '0',
                    ];
                    return Html::a('<span class="glyphicon glyphicon-trash">Delete</span>', $url, $options);
                },
                //view button
                'view' =>  function ($url, $model, $key) {
                        $options = [
                            'title' => Yii::t('yii', 'View'),
                            'aria-label' => Yii::t('yii', 'View'),
                            'data-pjax' => '0',
                        ];
                        return Html::a('<span class="glyphicon glyphicon-eye-open">View</span>', $url, $options);
                },
                //update button
                'update' =>  function ($url, $model, $key) {
                    $options = [
                        'title' => Yii::t('yii', 'Update'),
                        'aria-label' => Yii::t('yii', 'Update'),
                        'data-pjax' => '0',
                    ];
                    return Html::a('<span class="glyphicon glyphicon-pencil">Edit</span>', $url, $options);
                },
            ],
        ];
    }


}
