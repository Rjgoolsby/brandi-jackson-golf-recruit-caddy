<?php
namespace app\models\helpers;

use Yii;
use yii\base\Model;
use yii\helpers\FileHelper;
use yii\helpers\ArrayHelper;
use yii\base\UnknownPropertyException;
use yii\helpers\Html;
use yii\helpers\Url;

class HtmlParser extends Model{
	private $_dom;
	private $xpath;

	public function init(){
		parent::init();
	}

    public function setDom($file)
    {
    	$dom = new \DOMDocument;
        if(file_exists($file)){
        	@$dom->loadHTMLFile($file,LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        }else{
        	@$dom->loadHTML($file,LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        }
        $this->_dom = $dom;
        $this->xpath = new \DOMXPath($this->_dom);
	}

    public function getHtml()
    {
        if ($this->_dom === null) {
        	throw new UnknownPropertyException( "The Html Property Has Not Been Set For The Class" );
            // $this->setHtml();
        }
        return  $this->_dom->saveHtml();
    }

    public function getSchoolArray($gender=null,$level=null){
		$table =  $this->_dom->getElementById('SchoolList');
		Yii::trace($table,'dev');
		$rows = $table->getElementsByTagName('tr');
		// loop over the table rows
		foreach ($rows as $key => $row)
		{
			if ($key == 0) //or whatever
			continue;
			// get each column by tag name
			$cols = $row->getElementsByTagName('td');
			$url = trim($cols->item(1)->getElementsByTagName('a')->item(0)->getAttribute('href'));
			$query = parse_url($url, PHP_URL_QUERY);
			parse_str($query, $params);
			$id = $params['tid'];
			// echo the values
			$name = trim($cols->item(1)->nodeValue);
			$gender = (strpos($name, '(M)'))? 10 :20;
			$array[$id] = [
				'id' => $id,
				'url' => $url,
				'name' => trim($cols->item(1)->nodeValue),
				'city' => trim($cols->item(2)->nodeValue),
				'state' => trim($cols->item(3)->nodeValue),
				'level' => $level,
				'gender' => $gender,
			];
		}
		// 	ksort($array);
	 	return $array;
    }
}
