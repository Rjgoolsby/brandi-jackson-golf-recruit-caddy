<?php

namespace app\models\helpers;

use Yii;
// use yii\base\Model;
use yii\helpers\ArrayHelper;
use app\models\helpers\HtmlParser;


// Project models
use app\models\caddy\RecruitCaddy;
use app\models\caddy\RecruitCaddySearch;
use app\models\players\RecruitCaddyPlayers;
use app\models\schools\RecruitCaddySchools;
use app\models\players\RecruitCaddyPlayersSearch;
use app\models\schools\RecruitCaddySchoolsSearch;


class CaddyParser extends RecruitCaddySchools{

	public $schoolArray = [];
	public $log = [];

	public function updateSchools(Array $files,$store = false){
        $parser = new HtmlParser;
		$school = array();
		foreach ($files as $key => $file) {
        	$parser->setDom($file['path']);
			$list = $parser->getSchoolArray($file['gender'],$file['level']);
			foreach ($list as $val) {
				$school[] = $val;
			}
		}
		$this->schoolArray =  ArrayHelper::index($school, 'id');
		ksort($this->schoolArray);
		return $this->schoolArray;
		// return $this->schoolArray;
		// ksort($school);
		if($store){
			$this->_setSchools();
			return $this->log;
		}else{
			return $this->schoolArray;
		}
	}
  
	public function setSchools($schoolArray = []){
		$this->schoolArray = (empty($schoolArray))?$this->schoolArray:$schoolArray;
		$this->_setSchools();
		return $this->log;
	}

	protected function _setSchools(){

		if(empty($this->schoolArray)){
			$this->log[] = 'No Schools Found';
			return true;
		}
		foreach ($this->schoolArray as $key => $school) {
			$model = $this->findOne($key);
			if($model == null){
				$response = $this->newRecord($school);
			}else{
				$response = $this->updateRecord($model, $school);
			}
		}
	}

	protected function newRecord($record){
		$model = new RecruitCaddySchools;
		$model->load($record,'');
		// $model->id = $record['id'];
		if(!$model->save()){
			$this->log['err'][] = $model->errors;
			return false;
		}else{
			$this->log['success'][] = 'School ' . $record['name'] . ' Added';

		}
		return true;
	}

	protected function updateRecord($model,$record){
		$model->load($record,'');
		if(!$model->save()){
			$this->log['err'][] = $model->errors;
			return false;
		}else{
			$this->log['success'][] = 'School ' . $record['name'] . ' Updated';

		}
		return true;
	}


}
