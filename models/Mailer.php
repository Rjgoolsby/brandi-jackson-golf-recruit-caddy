<?php

/*
 * This file is part of the ptech project.
 *
 * (c) ptech project <http://github.com/ptech/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace models;

use Yii;
use yii\base\Model;
use app\models\helpers\Html2Text;
use yii\web\View;
use app\models\caddy\RecruitCaddy;
use app\models\caddy\RecruitCaddySearch;
/*

$template = EmailTemplates::find()->where(['default'=> TbmHelpers::getEmailMessagePrefsId('Reset Password')])->asArray()->one();
$messages = new \app\models\messages\Messages;
$newUser = \app\models\User::findUsersEmailApprovedUser($this->id);
//$users,Message id,from email, from name,
$messages->sendOne($newUser,$template['subject'],$template['id'],$template['from_email'],$template['from_name'],[
    'NEWUSERPASSWORD' => $this->password,
]);


*/

/**
 * Mailer.
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class Mailer extends Model
{
    /** @var string */
    public $viewPath = '@common/mail';

    /** @var string|array Default: `Yii::$app->params['adminEmail']` OR `no-reply@example.com` */
    public $sender = 'no-reply@pyrotechsolutions.com';

    /** @inheritdoc */
    public function init()
    {
        parent::init();
    }


/*    public function sendWelcomeMessage(User $user)
    {
        return $this->sendMessage($user->email,
            $this->getWelcomeSubject(),
            'welcome',
            ['user' => $user]
        );
    }
*/


    public function sendUpdates($id){
        // $model = new RecruitCaddy(['player_id'=>$id]);
        $searchModel = new RecruitCaddySearch(['player_id'=>$id]);
        $dataProvider = $searchModel->search([]);

        // Yii::trace(Yii::$app->wp->adminEmail,'dev');
        // $this->sender = 'developers@pyrotechsolutions.com';
        $this->sendMessage(Yii::$app->wp->adminEmail, $subject = $searchModel->user->display_name . ' Caddy Update!', 'brandiUpdate', $params=[
            'model'=>$model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
        return true;
    }

    public $_to;
    public function setTo($address = null){
        $this->_to = ($address == null)? Yii::$app->wp->adminEmail:$address;
    }
    public function getTo(){
        if(!isset($this->_to)){
            $this->setTo(); 
        }
        return $this->_to;
    }
    // public $from;
    public function sendNextStepMessageUpdate($message){

        $this->sendMessage($this->to, $message->display_name.' just sent you a message in Next Steps!!!', 'newMessage', $params=[
            'message'=>$message,
        ]);
        return true;
    }

    /**
     * @param string $to
     * @param string $subject
     * @param string $view
     * @param array  $params
     *
     * @return bool
     */
    protected function sendMessage($to, $subject, $view, $params = [])
    {
        /** @var \yii\mail\BaseMailer $mailer */
        $mailer = Yii::$app->mailer;
        $mailer->viewPath = $this->viewPath;
        $mailer->getView()->theme = Yii::$app->view->theme;

        $html = \Yii::$app->view->render($this->viewPath.'/'.$view,$params);
        $html2text = new Html2Text($html);
        $textHtml = $html2text->getText();
        // Yii::trace($this->sender,'dev');
        // Yii::trace($to,'dev');
        // return true;
        return Yii::$app->mailer->compose()
            ->setFrom($this->sender)
            ->setTo($to)
            ->setSubject($subject)
            ->setTextBody($textHtml)
            ->setHtmlBody($html)
            ->send();
    }

}
