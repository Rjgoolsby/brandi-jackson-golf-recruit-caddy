<?php

namespace app\models\message;

use Yii;
use yii\behaviors\TimestampBehavior;
use  app\models\User;
use  app\models\UserMeta;
use app\models\players\RecruitCaddyPlayers;
use app\models\schools\RecruitCaddySchools;
use app\models\caddy\RecruitCaddy;

/**
 * This is the model class for table "recruitMessageTable".
 *
 * @property integer $id
 * @property integer $player_id
 * @property integer $type
 * @property string $body
 * @property integer $created_at
 * @property integer $updated_at
 */
class RecruitMessageTable extends \yii\db\ActiveRecord
{
    const NEXT_STEP_MESSAGE = 10;

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recruitMessageTable';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['player_id','user_id','caddy_id', 'type'], 'required'],
            [['player_id','user_id','caddy_id', 'type', 'created_at', 'updated_at'], 'integer'],
            [['body'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'player_id' => 'Player ID',
            'user_id' => 'User ID',
            'caddy_id' => 'School ID',
            'type' => 'Type',
            'body' => 'Body',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    protected $_player;
    public function setPlayer(){
        $this->_player = $this->hasOne(User::className(), ['ID' => 'user_id']);
    }
    public function getPlayer(){
        if($this->_player == null){
            $this->setPlayer();
        }
        return $this->_player;
    }


    protected $_recepient;
    public function setRecepient(){
        $this->_recepient = $this->hasOne(RecruitCaddy::className(), ['player_id' => 'player_id']);
    }
    public function getRecepient(){
        if($this->_recepient == null){
            $this->setRecepient();
        }
        return $this->_recepient;
    }

/*    protected $_user;
    public function setUser(){
        $this->_user = $this->hasOne(User::className(), ['ID' => 'user_id']);
    }
    public function getUser(){
        if($this->_user == null){
            $this->setUser();
        }
        return $this->_user;
    }*/

    public function getUsermeta(){
        return $this->hasMany(UserMeta::className(), ['user_id' => 'user_id']);
    }

    protected $_meta;
    public function setMeta(){
        $meta = $this->hasOne(UserMeta::className(), ['user_id' => 'user_id'])
        // ->select('meta_value')
        ->where(['meta_key'=>['nickname','first_name','last_name','description']])->asArray()->all();
        $this->_meta = ArrayHelper::index($meta,'meta_key');
    }
    public function getMeta(){
        if($this->_meta == null){
            $this->setMeta();
        }
        return $this->_meta;
    }


    public function getLast_name(){
        return ArrayHelper::getValue($this->meta,'last_name.meta_value');
        // Yii::trace($this->player->getInfo('nickname')->asArray()->one(),'dev');
        return $this->player->getInfo('last_name')->asArray()->one()['meta_value'];
    }
    public function getFirst_name(){
        return ArrayHelper::getValue($this->meta,'first_name.meta_value');
        // Yii::trace($this->player->getInfo('first_name')->asArray()->one(),'dev');
        return $this->player->getInfo('first_name')->asArray()->one()['meta_value'];
    }

    public function getPlayerEmail(){
        return $this->recepient->user->user_email;
    }

    public function getRecepientDisplay_name(){
        return $this->recepient->user->display_name;
    }

    public function getDisplay_name(){
        return $this->player->display_name;
    }

    public function getNickname(){
        // Yii::trace(ArrayHelper::getValue($this->meta,'nickname.meta_value'),'dev');
        return ArrayHelper::getValue($this->meta,'nickname.meta_value');
    }

    public function getCaddy(){
        return $this->hasOne(RecruitCaddy::className(),['id'=>'caddy_id']);
    }

    public function getSchool(){
        return $this->caddy->school;
        // return $this->hasOne(RecruitCaddySchools::className(),['id'=>'caddy_id']);
    }
    public function getSchoolName(){
        return $this->caddy->schoolName;
    }
}
