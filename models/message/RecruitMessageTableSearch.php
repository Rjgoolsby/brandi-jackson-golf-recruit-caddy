<?php

namespace app\models\message;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\message\RecruitMessageTable;

/**
 * RecruitMessageTableSearch represents the model behind the search form about `app\models\message\RecruitMessageTable`.
 */
class RecruitMessageTableSearch extends RecruitMessageTable
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id','user_id', 'player_id', 'type', 'created_at', 'updated_at','caddy_id'], 'integer'],
            [['body'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RecruitMessageTable::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions,'caddy_id'
        $query->andFilterWhere([
            'id' => $this->id,
            'player_id' => $this->player_id,
            'user_id' => $this->user_id,
            'caddy_id' => $this->caddy_id,
            'type' => $this->type,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'body', $this->body]);

        return $dataProvider;
    }
}
