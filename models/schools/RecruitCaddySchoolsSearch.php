<?php

namespace app\models\schools;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\schools\RecruitCaddySchools;

/**
 * RecruitCaddySchoolsSearch represents the model behind the search form about `app\models\schools\RecruitCaddySchools`.
 */
class RecruitCaddySchoolsSearch extends RecruitCaddySchools
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'url', 'city', 'state','id', 'level', 'gender'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params=[])
    {
        // Yii::trace($params,'dev');
        $query = RecruitCaddySchools::find();

        // add conditions that should always apply here

        /* $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]); */

        $this->load($params,'');

        /* if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        } */

        // grid filtering conditions
        $query->andFilterWhere([ 
            'level' => $this->level,
            'gender' => $this->gender,
        ]);

        $query->andFilterWhere(['in', 'id', $this->id])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'state', $this->state]);
 
        
        $response =  $query->indexBy('id')->asArray()->all();
        // Yii::trace($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql,'dev');
        // Yii::trace($response,'dev');
        return $response;
    }
}
