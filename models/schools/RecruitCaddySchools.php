<?php

namespace app\models\schools;

use Yii;

/**
 * This is the model class for table "recruitCaddySchools".
 *
 * @property integer $id
 * @property string $name
 * @property string $url
 * @property string $city
 * @property string $state
 * @property integer $level
 * @property integer $gender
 */
class RecruitCaddySchools extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recruitCaddySchools';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id','name', 'url', 'city', 'state', 'level', 'gender'], 'required'],
            [['id','gender'], 'integer'],
            [['level','name', 'url', 'city'], 'string', 'max' => 100],
            [['state'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'url' => 'Url',
            'city' => 'City',
            'state' => 'State',
            'level' => 'Level',
            'gender' => 'Gender',
        ];
    }
}
