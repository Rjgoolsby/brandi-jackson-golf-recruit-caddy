<?php

namespace app\models\transactions;

use Yii;
use yii\behaviors\TimestampBehavior;
use app\models\players\RecruitCaddyPlayers;
use app\models\schools\RecruitCaddySchools;
use  app\models\User;
use  app\models\UserMeta;

/**
 * This is the model class for table "recruitCaddyTransactions".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $player_id
 * @property string $changes
 * @property integer $sent_at
 * @property integer $created_at
 * @property integer $updated_at
 */
class RecruitCaddyTransactions extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recruitCaddyTransactions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'player_id', 'changes'], 'required'],
            [['user_id', 'player_id', 'sent_at', 'created_at', 'updated_at'], 'integer'],
            [['changes'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'player_id' => 'Player ID',
            'changes' => 'Changes',
            'sent_at' => 'Sent At',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }


    protected $_player;
    public function setPlayer(){
        $this->_player = $this->hasOne(User::className(), ['ID' => 'user_id']);
    }
    public function getPlayer(){
        if($this->_player == null){
            $this->setPlayer();
        }
        return $this->_player;
    }
    public function getEditor(){
        return $this->player->display_name;
    }

}
