<?php

namespace app\models\players;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\players\RecruitCaddyPlayers;

/**
 * RecruitCaddyPlayersSearch represents the model behind the search form about `app\models\players\RecruitCaddyPlayers`.
 */
class RecruitCaddyPlayersSearch extends RecruitCaddyPlayers
{

    public $first_name;
    public $last_name;
    public $display_name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id','type'], 'integer'],
            [['first_name','last_name','display_name'],'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        // $query = RecruitCaddyPlayers::find();
        /*RecruitCaddyPlayers::find()
        ->joinWith(['player','player.meta'])
        ->where(['recruitCaddyPlayers.user_id'=>34])
        ->asArray()
        ->one();*/
        $query = RecruitCaddyPlayers::find()
        /*->select([
            '{{recruitCaddyPlayers}}.*', // select all customer fields
            'wp_usermeta.meta_value
        ])*/
        ->joinWith(['player']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        // Important: here is how we set up the sorting
        // The key is the attribute name on our "TourSearch" instance
        $dataProvider->sort->attributes['display_name'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['wp_users.display_name' => SORT_ASC],
            'desc' => ['wp_users.display_name' => SORT_DESC],
        ];
        // Lets do the same with country now
        /*$dataProvider->sort->attributes['first_name'] = [
            'asc' => ['wp_usermeta.first_name' => SORT_ASC],
            'desc' => ['wp_usermeta.first_name' => SORT_DESC],
        ];*/
        // Lets do the same with country now
        /*$dataProvider->sort->attributes['last_name'] = [
            'asc' => ['wp_usermeta.last_name' => SORT_ASC],
            'desc' => ['wp_usermeta.last_name' => SORT_DESC],
        ];*/
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'recruitCaddyPlayers.id' => $this->id,
            'recruitCaddyPlayers.user_id' => $this->user_id,
            'recruitCaddyPlayers.type' => $this->type,
        ]);
        $query->andFilterWhere(['like', 'wp_users.display_name', $this->display_name]);
        // $query->andFilterWhere(['like', 'wp_usermeta.last_name', $this->last_name]);
        // $query->andFilterWhere(['like', 'wp_usermeta.first_name', $this->first_name]);

        // Yii::trace($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql,'dev');
        return $dataProvider;
    }
}
