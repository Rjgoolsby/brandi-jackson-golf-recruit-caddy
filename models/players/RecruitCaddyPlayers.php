<?php

namespace app\models\players;

use Yii;
use yii\helpers\ArrayHelper;
use  app\models\User;
use  app\models\UserMeta;
/**
 * This is the model class for table "recruitCaddyPlayers".
 *
 * @property integer $id
 * @property integer $user_id
 */
class RecruitCaddyPlayers extends \yii\db\ActiveRecord
{
/*    const SUBSCRIPTION_PLANS = [
        10 => 'Basic',
        20 => 'Advanced',
    ];*/

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recruitCaddyPlayers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['user_id','required','message'=>'Please Select Player To Add'],
            ['type','required','message'=>'Please Select A Subscription Plan'],
            [['id','user_id'],'integer'],
            ['user_id', 'unique','message'=>'This player has already been added'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'type' => 'Player Subscription Plan',
        ];
    }

        protected $_player;
        public function setPlayer(){
            $this->_player = $this->hasOne(User::className(), ['ID' => 'user_id']);
        }
        public function getPlayer(){
            if($this->_player == null){
                $this->setPlayer();
            }
            return $this->_player;
        }

        public function getUsermeta(){
            return $this->hasMany(UserMeta::className(), ['user_id' => 'user_id']);
        }

        protected $_meta;
        public function setMeta(){
            $meta = $this->hasOne(UserMeta::className(), ['user_id' => 'user_id'])
            // ->select('meta_value')
            ->where(['meta_key'=>['nickname','first_name','last_name','description']])->asArray()->all();
            $this->_meta = ArrayHelper::index($meta,'meta_key');
        }
        public function getMeta(){
            if($this->_meta == null){
                $this->setMeta();
            }
            return $this->_meta;
        }


        public function getLast_name(){
            return ArrayHelper::getValue($this->meta,'last_name.meta_value');
            // Yii::trace($this->player->getInfo('nickname')->asArray()->one(),'dev');
            return $this->player->getInfo('last_name')->asArray()->one()['meta_value'];
        }
        public function getFirst_name(){
            return ArrayHelper::getValue($this->meta,'first_name.meta_value');
            // Yii::trace($this->player->getInfo('first_name')->asArray()->one(),'dev');
            return $this->player->getInfo('first_name')->asArray()->one()['meta_value'];
        }
        public function getDisplay_name(){
            return $this->player->display_name;
        }

        public function getPlayerId(){
            return $this->player->ID;
        }
        public function getNickname(){
            // Yii::trace(ArrayHelper::getValue($this->meta,'nickname.meta_value'),'dev');
            return ArrayHelper::getValue($this->meta,'nickname.meta_value');
        }
}
