<?php

namespace app\models\caddy;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\caddy\RecruitCaddy;

/**
 * RecruitCaddySearch represents the model behind the search form about `app\models\caddy\RecruitCaddy`.
 */
class RecruitCaddySearch extends RecruitCaddy
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'player_id', 'eval_status','school_id', 'intro_email_sent', 'intro_call_made', 'follow_email_sent', 'follow_call_made', 'visit', 'thank_you_card_mailed', 'offer_received', 'deleted_by','player_rating', 'created_at', 'updated_at'], 'integer'],
            [['likes', 'dislikes', 'next_steps'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $query = RecruitCaddy::find()->joinWith('school')->orderBy([
            'recruitCaddy.player_rating'=> SORT_DESC,
            'recruitCaddySchools.name' => SORT_ASC,
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['schoolName'] = [
            'asc' => ['recruitCaddySchools.name' => SORT_ASC],
            'desc' => ['recruitCaddySchools.name' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['player_rating'] = null;


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'recruitCaddy.id' => $this->id,
            'recruitCaddy.player_id' => $this->player_id,
            'recruitCaddy.school_id' => $this->school_id,
            'recruitCaddy.intro_email_sent' => $this->intro_email_sent,
            'recruitCaddy.intro_call_made' => $this->intro_call_made,
            'recruitCaddy.follow_email_sent' => $this->follow_email_sent,
            'recruitCaddy.follow_call_made' => $this->follow_call_made,
            'recruitCaddy.visit' => $this->visit,
            'recruitCaddy.thank_you_card_mailed' => $this->thank_you_card_mailed,
            'recruitCaddy.offer_received' => $this->offer_received,
            'recruitCaddy.deleted_by' => $this->deleted_by,
            // 'recruitCaddy.player_rating' => $this->player_rating,
            'recruitCaddy.created_at' => $this->created_at,
            'recruitCaddy.updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'recruitCaddySchools.name', $this->schoolName]);

        return $dataProvider;
    }
}
