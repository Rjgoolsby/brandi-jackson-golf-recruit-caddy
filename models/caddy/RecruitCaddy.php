<?php

namespace app\models\caddy;

use Yii;
use yii\behaviors\TimestampBehavior;
use app\models\players\RecruitCaddyPlayers;
use app\models\schools\RecruitCaddySchools;

/**
 * This is the model class for table "recruitCaddy".
 *
 * @property integer $id
 * @property integer $player_id
 * @property integer $school_id
 * @property integer $intro_email_sent
 * @property integer $intro_call_made
 * @property integer $follow_email_sent
 * @property integer $follow_call_made
 * @property integer $visit
 * @property integer $thank_you_card_mailed
 * @property integer $offer_received
 * @property string $likes
 * @property string $dislikes
 * @property string $next_steps
 * @property integer $deleted_by
 * @property integer $created_at
 * @property integer $updated_at
 */
class RecruitCaddy extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recruitCaddy';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['player_id', 'required'],
            ['school_id','required','message'=>'Please Select A School Required'],
            [['player_id', 'school_id'], 'unique', 'targetAttribute' => ['player_id', 'school_id'],'message'=>'This User already has this school in their Caddy'],
            ['player_rating','default', 'value' => 0],
            [['player_id','eval_status', 'school_id', 'intro_email_sent', 'intro_call_made', 'follow_email_sent', 'follow_call_made', 'visit', 'thank_you_card_mailed', 'offer_received', 'deleted_by', 'player_rating','created_at', 'updated_at'], 'integer'],
            [['likes', 'dislikes'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'player_id' => 'Player ID',
            'school_id' => 'School ID',
            'intro_email_sent' => 'Intro Email Sent',
            'intro_call_made' => 'Intro Call Made',
            'follow_email_sent' => 'Follow Email Sent',
            'follow_call_made' => 'Follow Call Made',
            'visit' => 'Visit',
            'thank_you_card_mailed' => 'Thank You Card Mailed',
            'offer_received' => 'Offer Received',
            'likes' => 'Likes',
            'dislikes' => 'Dislikes',
            'player_rating' => 'Player Rating',
            'deleted_by' => 'Deleted By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    public function getUser(){
        return $this->player->player;
    }
    public function getPlayer(){
        return $this->hasOne(RecruitCaddyPlayers::className(),['id'=>'player_id']);
    }

    public function getSchool(){
        return $this->hasOne(RecruitCaddySchools::className(),['id'=>'school_id']);
    }
    public function getSchoolName(){
        return $this->school->name;
    }
    public function getDisplay_name(){
        return $this->player->display_name;
    }
    public function getPlayerId(){
        return $this->player->playerId;
    }
    public function deactivate(){
        $this->deleted_by = Yii::$app->wp->id;
        $this->save();
    }
}
