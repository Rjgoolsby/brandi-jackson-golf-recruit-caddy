<?php

namespace ptech\mailgun;


use yii\base\NotSupportedException;
use yii\helpers\VarDumper;
use yii\mail\BaseMessage;
use Mailgun\Messages\MessageBuilder;

/**
 * Message implements a message class based on Mailgun.
 */
class Message extends BaseMessage
{
    /**
     * @var MessageBuilder Mailgun message builder.
     */
    private $_messageBuilder;
     /**
     * @var MessageBuilder Charset builder.
     */
    private $_charset;



    /**
     * @return MessageBuilder Mailgun message builder.
     */
    public function getMessageBuilder(){
        if (!is_object($this->_messageBuilder)) {
            $this->_messageBuilder = $this->createMessageBuilder();
        }
        return $this->_messageBuilder;
    }

    /**
     * @inheritdoc
     */
    public function getCharset()
    {
        if($this->isEmpty($this->_charset)){
            $this->setCharset('utf8');
        }
        return $this->_charset;
    }

    /**
     * @inheritdoc
     */
    public function setCharset($charset)
    {
        $this->_charset = $charset;
    }

    /**
     * @inheritdoc
     */
    public function getFrom()
    {
        $message = $this->getMessageBuilder()->getMessage();
        return !empty($message['from']) ? $message['from'] : null;
    }

    /**
     * @inheritdoc
     */
    public function setFrom($from)
    {
        if (is_array($from)) {
            foreach ($from as $email => $sender) {
                $this->getMessageBuilder()->setFromAddress($email, ['name' => $sender]);
            }
        } else {
            $this->getMessageBuilder()->setFromAddress($from);
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getReplyTo()
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function setReplyTo($replyTo)
    {
        if (is_array($replyTo)) {
            foreach ($replyTo as $email => $sender) {
                $this->getMessageBuilder()->setReplyToAddress($email, ['name' => $sender]);
            }
        } else {
            $this->getMessageBuilder()->setReplyToAddress($replyTo);
        }

        return $this;
    }


    /**
     * @inheritdoc
     */
    public function getTo()
    {
        $message = $this->getMessageBuilder()->getMessage();
        return !empty($message['to']) ? $message['to'] : null;
    }

    /**
     * @inheritdoc
     */
    public function setTo($to)
    {
        if (is_array($to)) {
            foreach ($to as $email => $recepient) {
                $this->getMessageBuilder()->addToRecipient($email, ['name' => $recepient]);
            }
        } else {
            $this->getMessageBuilder()->addToRecipient($to);
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getCc()
    {
        $message = $this->getMessageBuilder()->getMessage();
        return !empty($message['cc']) ? $message['cc'] : null;
    }

    /**
     * @inheritdoc
     */
    public function setCc($cc)
    {
        $this->getMessageBuilder()->addCcRecipient($cc);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getBcc()
    {
        $message = $this->getMessageBuilder()->getMessage();
        return !empty($message['bcc']) ? $message['bcc'] : null;
    }

    /**
     * @inheritdoc
     */
    public function setBcc($bcc)
    {
        $this->getMessageBuilder()->addBccRecipient($bcc);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getSubject()
    {
        $message = $this->getMessageBuilder()->getMessage();
        return !empty($message['subject']) ? $message['subject'] : null;
    }

    /**
     * @inheritdoc
     */
    public function setSubject($subject)
    {
        $this->getMessageBuilder()->setSubject($subject);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setTextBody($text)
    {
        $this->getMessageBuilder()->setTextBody($text);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setHtmlBody($html)
    {
        $this->getMessageBuilder()->setHtmlBody($html);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function attach($fileName, array $options = [])
    {
        $attachmentName = !empty($options['fileName']) ? $options['fileName'] : null;
        $this->getMessageBuilder()->addAttachment("@{$fileName}", $attachmentName);

        return $this;
    }

    /**
     * @param string      $timeDate
     * @param string|null $timeZone
     *
     * @return 
     */
    public function setDeliveryTime($time,$timezone)
    {
        $this->getMessageBuilder()->setDeliveryTime($time, $timezone);

        return $this;
    }
    /**
     * @inheritdoc
     */
    public function attachContent($content, array $options = [])
    {
        throw new NotSupportedException();
    }

    /**
     * @inheritdoc
     */
    public function embed($fileName, array $options = [])
    {
        throw new NotSupportedException();
    }

    /**
     * @inheritdoc
     */
    public function embedContent($content, array $options = [])
    {
        throw new NotSupportedException();
    }

    /**
     * @inheritdoc
     */
    public function toString()
    {
        return VarDumper::dumpAsString($this->getMessageBuilder()->getMessage());
    }

    /**
     * Creates the Mailgun message builder.
     * @return MessageBuilder message builder.
     */
    protected function createMessageBuilder()
    {
        return new MessageBuilder;
    }

    /**
     * Check if a variable is empty or not set.
     *
     * @param mixed $var variable to perform the check
     *
     * @return boolean
     */
    protected function isEmpty($var)
    {
        return !isset($var) ? true : (is_array($var) ? empty($var) : ($var === null || $var === ''));
    }
}
