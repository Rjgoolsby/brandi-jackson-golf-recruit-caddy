
var Template =
`
<div>
<!-- <pre>{{ pagination }}</pre> -->

<v-dialog v-model="dialog.status" persistent max-width="600px">
    <v-card  class="clear-forms">
        <v-toolbar  color="indigo" dark>
            <v-toolbar-title>{{ dialog.title }}</v-toolbar-title>
        </v-toolbar>
        <v-card-text >
            <v-container px-0 fluid >
                    <!-- Type 10 is Likes Dislikes -->
                    <v-flex xs12 v-show="dialog.type == 10">
                        <v-flex xs12>
                            <h3>Likes</h3>
                            <v-textarea
                                v-model=dialog.likes
                                auto-grow
                                box
                                placeholder="Likes"
                            ></v-textarea>
                        </v-flex>
                        <v-flex xs12>
                            <h3>Dislike</h3>
                            <v-textarea
                                v-model=dialog.dislikes
                                auto-grow
                                box
                                placeholder="Dislikes"
                            ></v-textarea>
                        </v-flex>
                    </v-flex>
                    <!-- Type 20 is Next Steps -->
                    <v-flex xs12 v-show="dialog.type == 20">
                        <v-card flat>
                            <v-card-text ref="nextStepsContainer" style="height:300px; overflow-y:scroll;border:1px solid grey;" >
                                <v-flex xs12  v-for="(message, index) in nextStepsMessages" :key="message.timestamp">
                                    <v-hover>
                                        <v-system-bar lights-out status class="px-0" slot-scope="{ hover }">
                                            <span class="body-2 black--text">{{ message.sender }}</span>
                                            <v-spacer></v-spacer>
                                            <span class="caption ">{{ message.timestamp |  parseTimestampDate }}</span>
                                            <v-icon v-show="hover && message.author == authorId" @click="editMessage(index)" color="blue">edit</v-icon>
                                            <v-icon v-show="hover && message.author == authorId" @click="deleteMessage(index)" color="red">delete_forever</v-icon>
                                        </v-system-bar>
                                    </v-hover>
                                    <v-flex  v-html="message.body"></v-flex >
                                    <v-divider v-if="index + 1 < nextStepsMessages.length" :key="index"/>
                                </v-flex>
                            </v-card-text>
                            <v-card-text class="px-0" >
                                <v-textarea
                                    v-model=nextStepEntryBox
                                    counter
                                    full-width
                                    clearable
                                    hide-details
                                    auto-grow
                                    outline
                                    rows=2
                                    placeholder="Message"
                                >
                                    <template v-slot:append-outer>
                                        <v-icon @click="editingMessageIndex != undefined ? resaveMessage(): sendMessage() " color="green">send</v-icon>
                                    </template>
                                </v-textarea>
                            </v-card-text>
                        </v-card>
                    </v-flex>
            </v-container>
        </v-card-text>
        <v-divider />
        <v-card-actions>
            <v-spacer></v-spacer>
            <v-btn color="blue darken-1" flat @click="closeDialog">Close</v-btn>
            <v-btn color="blue darken-1" v-show="dialog.type == 10" flat @click="saveDialog">Save</v-btn>
        </v-card-actions>
    </v-card>
</v-dialog>
<v-card-title>
    Current Schools
    <v-spacer></v-spacer>
    <v-text-field
        v-model="search"
        append-icon="search"
        label="Search"
        single-line
        hide-details
        clearable
    ></v-text-field>
</v-card-title>
<v-alert
      :value="readOnly"
      type="warning"
    >
      The table is currently Sorted by a column. Due to data integrity, editing the table while in sort mode is disabled. Reset sort to edit table.
      <v-btn @click="resetSort">Reset Sorting</v-btn>
</v-alert>
<v-data-table
    :loading="loading"
    :headers="headers"
    :items="playerCaddy"
    :search="search"
    :rows-per-page-items="rowsPerPageItems"
    :pagination.sync="pagination"
    disable-initial-sort
    class="elevation-1 caddy-table">
    <template slot="no-data">
        <v-alert :value="true" color="error" icon="warning">
            Sorry, nothing to display here :(
        </v-alert>
    </template>
    <template slot="items" slot-scope="props">
        <tr :class="['row-class-' + props.item.eval_status]">
            <td class="pl-3">
                <v-flex xs12>
                    <p class="link" @click="getSchoolLink(props.item.school_id)">{{ getIndex(props.index) }}. {{ props.item.schoolName }}</p>
                    <!-- <p :class="{link:saveTransaction}" @click="saveTransaction ? getSchoolLink(props.item.school_id): ''">{{ getIndex(props.index) }}. {{ props.item.schoolName }}</p> -->
                </v-flex>
                <!-- <v-flex xs12>
                    <v-radio-group hide-details
                        :readonly="readOnly"
                        v-model="props.item.eval_status"
                        @change="updateVal(props.item,'eval_status')"
                    >
                        <v-radio v-for="(decision,index) in evalList" :key="index"
                            :label="decision.label"
                            :value="decision.value"
                            :color="decision.color"
                        />
                    </v-radio-group>
                </v-flex> -->
            </td>
            <td class="pl-3">
                <v-flex xs12>
                    <v-radio-group hide-details
                    :readonly="readOnly"
                    v-model="props.item.eval_status"
                    @change="updateVal(props.item,'eval_status')"
                >
                    <v-radio v-for="(decision,index) in evalList" :key="index"
                        :label="decision.label"
                        :value="decision.value"
                        :color="decision.color"
                    />
                </v-radio-group>
                </v-flex>
            </td>
            <td class="pl-3">
                <v-flex xs12>
                    <v-slider
                        :readonly="readOnly"
                        v-model="props.item.player_rating"
                        thumb-label="always"
                        @change="updateVal(props.item,'player_rating')"
                    ></v-slider>
                </v-flex>
            </td>
            <td class="pl-3 text-xs-center">
                <v-btn @click="props.item.intro_email_sent = toggleVal(props.item,'intro_email_sent')" :color="props.item.intro_email_sent == 1 ? 'success' : 'error'" fab small dark>
                    <span v-if="props.item.intro_email_sent == 1" >Yes</span>
                    <span v-else>No</span>
                </v-btn>
            </td>
            <td class="pl-3 text-xs-center">
                <v-btn @click="props.item.intro_call_made = toggleVal(props.item,'intro_call_made',)" :color="props.item.intro_call_made == 1 ? 'success' : 'error'" fab small dark>
                    <span v-if="props.item.intro_call_made == 1" >Yes</span>
                    <span v-else>No</span>
                </v-btn>
            </td>
            <td class="pl-3 text-xs-center">
                <v-btn @click="props.item.follow_email_sent = toggleVal(props.item,'follow_email_sent')" :color="props.item.follow_email_sent == 1 ? 'success' : 'error'" fab small dark>
                    <span v-if="props.item.follow_email_sent == 1" >Yes</span>
                    <span v-else>No</span>
                </v-btn>
            </td>
            <td class="pl-3 text-xs-center">
                <v-btn @click="props.item.follow_call_made = toggleVal(props.item,'follow_call_made')" :color="props.item.follow_call_made == 1 ? 'success' : 'error'" fab small dark>
                    <span v-if="props.item.follow_call_made == 1" >Yes</span>
                    <span v-else>No</span>
                </v-btn>
            </td>
            <td class="pl-3 text-xs-center">
                <v-btn @click="props.item.visit = toggleVal(props.item,'visit')" :color="props.item.visit == 1 ? 'success' : 'error'" fab small dark>
                    <span v-if="props.item.visit == 1" >Yes</span>
                    <span v-else>No</span>
                </v-btn>
            </td>
            <td class="pl-3 text-xs-center">
                <v-btn @click="props.item.offer_received = toggleVal(props.item,'offer_received')" :color="props.item.offer_received == 1 ? 'success' : 'error'" fab small dark>
                    <span v-if="props.item.offer_received == 1" >Yes</span>
                    <span v-else>No</span>
                </v-btn>
            </td>
            <td class="pl-3 text-xs-center">{{  props.item.updated_at | parseUnixDate }}</td>
            <td class="px-3 text-xs-center">
                <v-btn @click="editDialog(props.item,20)" flat small color="blue" class="my-0">Next Steps</v-btn>
                <br>
                <v-btn @click="editDialog(props.item,10)" flat small color="green" class="my-0">Likes/Dislikes</v-btn>
                <br>
                <v-btn @click.stop="deleteSchool(props.item.school_id,props.item)" flat small color="red"class="my-0" >
                    <v-icon>delete_forever</v-icon> Delete
                </v-btn>
            </td>
        </tr>
    </template>
</v-data-table>
</div>
`

export default {
    name:'caddy-review',
    template: Template,
    props:{
        userId:{
            required:true,
        },
        isAdmin:{
            default:true,
        },
        subType:{
            default:10,
        }
    },
    data() {
        return {
            dialog:{
                status:false,
                title:'',
                type: '',
                likes: '',
                dislikes: '',
                recordId:'',
                nextSteps:[],
                el:undefined
            },
            editingMessageIndex:undefined,
            nextStepEntryBox:'',
            loading:true,
            search: '',
            headers :[
                {
                    text: 'School name',
                    align: 'left',
                    sortable: true,
                    value:'schoolName', //eval_status or schoolName
                    class:"pl-3",
                    width:'180px'
                },
                {
                    text: 'School Evaluation',
                    align: 'left',
                    sortable: true,
                    value:'eval_status', //eval_status or schoolName
                    class:"pl-3",
                },
                {
                    text:'Player Rating',
                    sortable: true,
                    value: 'player_rating',
                    // class:"pl-3",
                    align:'center'
                },
                {
                    text:'Intro Emal',
                    sortable: true,
                    value: 'intro_email_sent',
                    // class:"pl-3",
                    align:'center'
                },
                {
                    text:'Intro Call',
                    sortable: true,
                    value: 'intro_call_made',
                    // class:"pl-3",
                    align:'center'
                },
                {
                    text:'Follow Up Email',
                    sortable: true,
                    value: 'follow_email_sent',
                    // class:"pl-3",
                    align:'center'
                },
                {
                    text:'Follow Up Call',
                    sortable: true,
                    value: 'follow_call_made',
                    // class:"pl-3",
                    align:'center'
                },
                {
                    text:'Visit?',
                    sortable: true,
                    value: 'visit',
                    // class:"pl-3",
                    align:'center'
                },
                {
                    text:'Offer',
                    sortable: true,
                    value: 'offer_received',
                    // class:"pl-3",
                    align:'center'
                },
                {
                    text:'Last Updated',
                    sortable: true,
                    value: 'updated_at',
                    // class:"pl-3",
                    align:'center'
                },
                {
                    text:'Actions',
                    sortable: false,
                    class:"pa-3",
                    align:'center'
                },
            ],
            rowsPerPageItems: [10,10,20, 40,{"text":"$vuetify.dataIterator.rowsPerPageAll","value":-1}],
            pagination: {
                rowsPerPage: 10
            },
            evalList:[
                {
                    label:'Realistic',
                    value:10,
                    color:'#66ff66',
                },
                {
                    label:'Fallback',
                    value:20,
                    color:'#e8e24a'
                },
                {
                    label:'Dream',
                    value:30,
                    color:'#ff4d4d',
                }
            ],
            dbRef:undefined,
            playerCaddyDataset:[],
            playerCaddy:[],
        }
    },
    async created(){
        // var snapshot = await firebase.database().ref(`players/${this.userId}/subscription`).once('value');

        // console.log(snapshot);
        // this.subId = snapshot.val();
        // console.log(`User SubId is ${this.subId}`);
    },
    mounted(){
        var vm = this;
        console.log(caddyOptions);
        this.$nextTick(function(){
            console.log(vm.saveTransaction);
            if(vm.userId != undefined){
                vm.dbRef = firebase.database().ref(`caddys/${this.userId}`);
                vm.dbRef.on('value',async (snapshot)=>{

                    vm.loading = true;
                    vm.playerCaddyDataset = [];
                    // console.log('caddy snapshot',snapshot.val());
                    vm.playerCaddyDataset = snapshot.val();
                    vm.loading = false;
                    vm.setPlayerCaddy();
                })
            }
        })
    },
    filters:{
        parseTimestampDate(val){
            // console.log(val);
            return moment(val).format("M/D/YYYY h:mm:ss a");
        },
        parseUnixDate(val){
            // console.log(val);
            return moment.unix(val).format("M/D/YYYY h:mm:ss a");
        }
    },
    beforeDestroy(){
        if(this.dbRef != undefined){
            this.dbRef.off();
        }
    },
    computed:{
        readOnly(){
            return this.pagination.sortBy != null;
        },
        saveTransaction(){
            var status =  !this.isAdmin && this.subType == 20;
            return status;
        },
        name(){
            return  caddyOptions.name;
        },
        authorId(){
            return caddyOptions.userId;
        },
        nextStepsMessages(){
            var steps =  ( this.dialog.status && this.dialog.type == 20 && this.playerCaddyDataset[this.dialog.recordId].hasOwnProperty('nextSteps')) ? this.playerCaddyDataset[this.dialog.recordId].nextSteps : [];
            this.scrollToEnd();
            return steps;
        }
    },
    watch:{
        saveTransaction(newVal){
            console.log(`saveTrans ${newVal}`)
        }
    },
    methods: {
        resetSort(){
            this.pagination.sortBy = null;
            this.pagination.descending = null;
        },
        deleteMessage(index){
            var vm = this;
            this.$confirm('Do you really want to delete this Message?',{
                buttonTrueText: 'Delete Message',
                buttonFalseText: 'Keep MEssage',
                buttonFalseColor: 'green',
                buttonTrueColor: 'red',
                property: '$confirm'
            }).then(res => {
                if(res){
                    vm.$delete(this.playerCaddyDataset[this.dialog.recordId].nextSteps, index);
                    var schoolId = this.dialog.recordId;
                    firebase.database().ref(`caddys/${this.userId}/${schoolId}/nextSteps/${index}`).remove();
                }
            })
        },
        editMessage(index){
            this.editingMessageIndex = index;
            this.nextStepEntryBox = this.playerCaddyDataset[this.dialog.recordId].nextSteps[index].body;
        },
        resaveMessage(){
            console.log('resaving message');
            // this.playerCaddyDataset[this.dialog.recordId].nextSteps[this.editingMessageIndex].body = this.nextStepEntryBox;
            console.log(`caddys/${this.userId}/${this.dialog.recordId}/nextSteps/${this.editingMessageIndex}`);
            firebase.database().ref(`caddys/${this.userId}/${this.dialog.recordId}/nextSteps/${this.editingMessageIndex}`).update({
                body:this.nextStepEntryBox,
            });
            this.editingMessageIndex = undefined;
            this.nextStepEntryBox = '';
        },
        sendMessage(){
            firebase.database().ref(`caddys/${this.userId}/${this.dialog.recordId}/nextSteps`).push().set({
                sender:this.name,
                body:this.nextStepEntryBox,
                timestamp:moment().valueOf(),
                author: this.authorId,
            });

            if(this.saveTransaction){
                firebase.database().ref(`transactions`).push().set({
                    timestamp: moment().unix(),
                    name: this.name,
                    school: this.dialog.record.schoolName,
                    data:this.nextStepEntryBox,
                    type:'nextSteps'
                });
            }
            this.nextStepEntryBox = '';

        },
        editDialog(record,type){

            if(this.readOnly){
                return;
            }
            console.log(record);
            if(type == 10){
                console.log(`Loading Edit/Dislikes for School ID ${record.school_id}`);
                this.dialog = {
                    status:true,
                    title:'Likes & Dislikes',
                    type: type,
                    likes: record.likes,
                    dislikes: record.dislikes,
                    recordId:record.school_id,
                    record:record,
                }
            }else if(type == 20){
                console.log(`Loading Next Steps for School ID ${record.school_id}`);
                var nextSteps = (record.nextSteps == undefined || record.nextSteps == null) ? [] : record.nextSteps
                this.dialog = {
                    status:true,
                    title:'Next Steps',
                    type: 20,
                    recordId:record.school_id,
                    nextSteps:nextSteps,
                    record:record,
                }
            }
        },
        closeDialog(){
            this.dialog = {
                status:false,
                title:'',
                type: '',
                likes: '',
                dislikes: '',
                recordId:'',
                author: '',
                record:undefined
            };
            this.nextStepEntryBox = '';
            this.editingMessageIndex = undefined;
        },
        saveDialog(){
            if(this.dialog.type == 10){
                // this.playerCaddyDataset[this.dialog.recordId].likes = this.dialog.likes;
                // this.playerCaddyDataset[this.dialog.recordId].dislikes = this.dialog.dislikes;
                var schoolId = this.dialog.recordId;
                var payload = {
                    likes:this.dialog.likes,
                    dislikes:this.dialog.dislikes,
                    updated_at: moment().unix()
                };
                console.log(payload);
                firebase.database().ref(`caddys/${this.userId}/${schoolId}`).update(payload);
                if(this.saveTransaction){
                    firebase.database().ref(`transactions`).push().set({
                        timestamp: moment().unix(),
                        name: this.name,
                        school: this.dialog.record.schoolName,
                        data:{
                            likes:this.dialog.likes,
                            dislikes:this.dialog.dislikes,
                        },
                        type:'likes/dislikes'
                    });
                }
            }
            this.closeDialog();
        },
        updateVal(record,key,valTest){
            var vm = this;
            var val = record[key];
            var schoolId = record.school_id
            console.log(record,key,val,valTest)
            var payload = {};
            payload[key] = parseInt(val);
            payload.updated_at = moment().unix();
            console.log(payload);
            firebase.database().ref(`caddys/${this.userId}/${schoolId}`).update(payload);
            if(this.saveTransaction){
                delete payload.updated_at;
                firebase.database().ref(`transactions`).push().set({
                    timestamp: moment().unix(),
                    school: record.schoolName,
                    name: vm.name,
                    data:payload,
                    type:'updateVal'
                });
            }
        },
        toggleVal(record,key){
            if(this.readOnly){
                return record[key];
            }
            var val = record[key];
            var saveVal = (val == 1) ? 0 : 1;
            record[key] = saveVal;
            this.updateVal(record,key);
        },
        getIndex(val){
            return ((this.pagination.page -1 )*this.pagination.rowsPerPage) + val+1;
        },
        async setPlayerCaddy(){
            const data = new URLSearchParams();
            for (const id in this.playerCaddyDataset) {
                data.append('id[]',id);
            }
            try {
                var response = await axios({
                    method:'POST',
                    url:'/wp-json/recruit-caddy/query-schools',
                    headers: {
                        'X-WP-Nonce': caddyOptions.nonce,
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data:data
                });
                console.log('setPlayerCaddy',response);
                this.playerCaddy = [];
                var tmp = [];
                for (const id in this.playerCaddyDataset) {
                    if (this.playerCaddyDataset.hasOwnProperty(id)) {
                        const record = this.playerCaddyDataset[id];
                        record.schoolName = {};
                        record.schoolName = response.data[id].name;
                        tmp.push(record);
                        // this.playerCaddy.push(record);
                    }
                }
                this.playerCaddy = tmp;

                // console.log('playerCaddy',this.playerCaddy);
            } catch (error) {
                console.log(error);
            }
        },
        async getLegacySchools(){
            var vm = this;
            jQuery.ajax( {
                url: '/wp-json/recruit-caddy/query-schools',
                method: 'POST',
                beforeSend: function ( xhr ) {
                    xhr.setRequestHeader( 'X-WP-Nonce', caddyOptions.nonce);
                },
            } ).done( function ( response ) {
                // console.log( 'CADDY Schools',response );
                vm.legacySchoolsData = response;
            } );
        },
        deleteSchool(id,el){
            if(this.readOnly){
                return;
            }
            console.log('Delete School',id);
            this.$confirm('Do you really want to delete this school?',{
                buttonTrueText: 'Delete School',
                buttonFalseText: 'Keep School',
                buttonFalseColor: 'green',
                buttonTrueColor: 'red',
                property: '$confirm'
            }).then(res => {
                if(res){
                    console.log('removing record => '+id);
                    firebase.database().ref(`caddys/${this.userId}/${id}`).remove();
                    if(this.saveTransaction){
                        firebase.database().ref(`transactions`).push().set({
                            timestamp: moment().unix(),
                            school: el.schoolName,
                            name: this.name,
                            data:`Deleted School ${el.schoolName}`,
                            type:'deleteSchool'
                        });
                    }
                }
            })
        },
        scrollToEnd: function() {
            var vm = this;
            if(this.$refs.nextStepsContainer){
                setTimeout(function(){
                    vm.$vuetify.goTo(vm.$refs.nextStepsContainer.scrollHeight, {
                        container:vm.$refs.nextStepsContainer,
                        duration: 0,
                        offset: 0,
                        easing: 'linear'
                    })
                },100);
            }
        },
        getSchoolLink(id){

            // if(this.saveTransaction){
                var vm = this;
                vm.loading = true;
                const data = new URLSearchParams();
                data.append('tid',id);

                axios({
                    method:'POST',
                    url: '/wp-json/recruit-caddy/get-school-information',
                    headers: {
                        'X-WP-Nonce': caddyOptions.nonce,
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data:data
                }).then( function ( response ) {
                    console.log('School link',response);
                    if(vm.validURL(response.data)){

                        vm.loading = false;
                        window.open(response.data, '_blank');
                    }
                } );
            // }
        },
        validURL(str) {
            var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
              '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
              '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
              '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
              '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
              '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
            return !!pattern.test(str);
          }
    }
  }