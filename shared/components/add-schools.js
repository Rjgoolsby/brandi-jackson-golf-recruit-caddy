
var Template =
`
<v-card>
    <v-card-text >
        <h3>Add School</h3>
        <v-autocomplete
            v-model="selection"
            :items="items"
            attach
            label="Select Schools"
            clearable
            hide-details
        >

        <template v-slot:append-outer>
            <v-btn @click="addSchool"> <v-icon  color="green">send</v-icon> Add </v-btn>
        </template>
        </v-autocomplete>
        <div class="mt-3">
            <label  v-show="!showFilters" @click="showFilters = true">Filter</label>
            <label  v-show="showFilters" @click="showFilters = false">Close Filter</label>
        </div>
    </v-card-text>
    <v-card-text  class="pt-0" v-show="showFilters">
        <v-container fluid  class="pt-0" >
            <v-layout row wrap>
                <v-flex xs12>
                    <v-checkbox v-for="(level,index) in levels" :key="index"
                        v-model="divisionFilters"
                        :label="level"
                        color="blue"
                        :value="level"
                        hide-details
                    ></v-checkbox>
                </v-flex>
            </v-layout>
        </v-container>
    </v-card-text>
</v-card>
`

export default {
    name:'add-schools',
    template: Template,
    props:{
        userId:{
            required:true,
        },
        isAdmin:{
            default:true,
        },
        subType:{
            default:10,
        }
    },
    data() {
        return {
            showFilters:false,
            selection:'',
            schools:[],
            levels: ["DivI", "DivII", "DivIII", "NCCAA", "USCAA", "NAIA"],
            divisionFilters:["DivI", "DivII", "DivIII", "NCCAA", "USCAA", "NAIA"],
        }
    },
    created(){
        this.getSchools();
    },
    mounted(){
        var vm = this;
        this.$nextTick(function(){

        })
    },
    computed:{
        saveTransaction(){
            var status =  !this.isAdmin && this.subType == 20;
            return status;
        },
        name(){
            return  caddyOptions.name;
        },
        items(){
            var list = [];
            var length = this.schools.length;
            // console.log(length);
            for (let i = 0; i < length; i++) {
                var school = this.schools[i];
                if(this.divisionFilters.includes(school.level)){
                    // console.log(this.genderFilter.includes(school.gender) && this.divisionFilters.includes(school.level));
                    list.push({
                        text:school.name,
                        value: school.id
                    })
                }
            }
            // console.log('made it');
            return list;
        }
    },
    methods: {
        getSchools(){
            var vm = this;
            jQuery.ajax( {
                url: '/wp-json/recruit-caddy/get-all-schools',
                method: 'GET',
                beforeSend: function ( xhr ) {
                    xhr.setRequestHeader( 'X-WP-Nonce', caddyOptions.nonce);
                },
            } ).done( function ( response ) {
                // var data = JSON.parse(response );
                console.log( 'CADDY Schools',response);
                vm.schools = response ;
            } );
        },
        addSchool(){
            var vm = this;
            var schoolName;
            var length = this.items.length;
            for (let index = 0; index < length; index++) {
                const school = this.items[index];
                if(school.value == this.selection){
                    schoolName = school.text;
                }

            }
            firebase.database().ref(`caddys/${this.userId}/${this.selection}`).once('value').then(function(snapshot){
                if(snapshot.val() == undefined || snapshot.val() == null){
                    firebase.database().ref(`caddys/${vm.userId}/${vm.selection}`).update({
                        created_at : moment().unix(),
                        eval_status : 10,
                        follow_call_made : 0,
                        follow_email_sent : 0,
                        intro_call_made : 0,
                        intro_email_sent : 0,
                        key : 0,
                        offer_received : 0,
                        player_rating : 0,
                        school_id : vm.selection,
                        thank_you_card_mailed : 0,
                        updated_at : moment().unix(),
                        visit : 0
                    });
                    if(vm.saveTransaction){
                        firebase.database().ref(`transactions`).push().set({
                            timestamp: moment().unix(),
                            name: vm.name,
                            school: schoolName,
                            data:`Added School ${schoolName}`,
                            type:'addedSchool'
                        });
                    }
                }


                vm.selection = "";
            })
        }
    }
  }
