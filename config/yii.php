<?php
$params = require __DIR__ . '/params.php';
return [
    'id' => 'recruit-caddy',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'params' => $params,
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'RR&X1Zi1AtQu2A!cU3C@MOyXGOtK$*1t',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ], 
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host='.DB_HOST.';dbname='.DB_NAME,
            'username' => DB_USER,
            'password' => DB_PASSWORD,
            'charset' => DB_CHARSET,
            'enableSchemaCache' => true,
            'schemaCacheDuration' => 60,
            'schemaCache' => 'cache'
        ], 
        'log' => [
            'traceLevel' => 3,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning']
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['trace'],
                    'categories' => ['dev'],
                    // 'logVars' => ['_GET','_POST'],
                    'logFile' => '@app/runtime/logs/trace.log',
                    'maxFileSize' => 1024 * 2,
                    'maxLogFiles' => 50,
                ]
                // [
                //     'class' => 'yii\log\FileTarget',
                //     'levels' => ['info'],
                //     'categories' => ['cron'],
                //     'logVars' => ['_GET','_POST'],
                //     'logFile' => '@app/runtime/logs/cron.log',
                //     'maxFileSize' => 1024 * 2,
                //     'maxLogFiles' => 50,
                // ],
            ],
        ],
    ], 
];