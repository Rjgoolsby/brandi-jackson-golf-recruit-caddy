var firebaseConfig = {
    apiKey: "AIzaSyAwlO4iyR0AZP8u-3V9ZBizMy5BcrhLiKc",
    authDomain: "brandijacksongolfrecruitcaddy.firebaseapp.com",
    databaseURL: "https://brandijacksongolfrecruitcaddy.firebaseio.com",
    projectId: "brandijacksongolfrecruitcaddy",
    storageBucket: "",
    messagingSenderId: "377095691093",
    appId: "1:377095691093:web:3a43f562dea161b4"
};

var userPassword = 'l5E$P@*7WuWlcg9HwVVT%wIfmOuYME&eipx4nzeVslS6tq^&aD' ;


// async function firebaseInit(){
//     console.log('initializing firebase');
//     // Initialize Firebase
//     await firebase.initializeApp(firebaseConfig); 
//     firebase.auth().onAuthStateChanged(async function(user){
//         var user = firebase.auth().currentUser;
//         var email = caddyOptions.email; 
//         console.log('user',user,caddyOptions);
//         if(!user && email){
//             await firebaseLogin(email);
//         } 
//         var user = firebase.auth().currentUser;
//         console.log('user',user); 
//     });
// }


async function firebaseLogin(email){
    console.log('Running firebaseLogin');
    try {
        await firebase.auth().setPersistence(firebase.auth.Auth.Persistence.NONE);
        await firebase.auth().signInWithEmailAndPassword(email, userPassword);
    } catch (error) {
        if(error.code == 'auth/user-not-found'){ 
            await _firebaseCreateNewUserAndLogin(email);  
        }else{
            console.log('Login Failed for unknown Reason');
        }
    }
}
async function _firebaseCreateNewUserAndLogin(email){
    try {
        await firebase.auth().createUserWithEmailAndPassword(email, userPassword)
    } catch (error) {
        console.log('Create New User Failed... Loggin in Anonymously');
        await _firebaseLoginInAnonymously(emaiL)
    }
}
async function _firebaseLoginInAnonymously(email){
    try {
        await firebase.auth().signInAnonymously();  
        await firebase.auth().currentUser.linkAndRetrieveDataWithCredential(firebase.auth.EmailAuthProvider.credential(email, userPassword));
    } catch (error) {
        console.log("Error upgrading anonymous account", error);
    }
}
export { firebaseConfig, firebaseLogin };