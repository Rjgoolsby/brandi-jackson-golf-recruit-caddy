<?php
if(defined('DB_HOST') && defined('DB_NAME') && defined('DB_USER') && defined('DB_PASSWORD') && defined('DB_CHARSET')){
    $db = [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host='.DB_HOST.';dbname='.DB_NAME,
            'username' => DB_USER,
            'password' => DB_PASSWORD,
            'charset' => DB_CHARSET,
            'enableSchemaCache' => true,
            'schemaCacheDuration' => 60,
            'schemaCache' => 'cache'
        ]
    ];
}else{
    $db = [];
}


return [   
    'id' => 'recruit-caddy-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => '\app\console\controllers',
    'params' => [],
    'components' => array_merge([
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'mailer' => [
            'class' => 'ptech\mailgun\Mailer',
            'domain' => 'mailgun.pyrotechsolutions.com',
            'key' => 'key-f2a1639449a6cc48a1fcb7b6ebb7b8d5',
        ],
        'log' => [
            'traceLevel' => 3,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning']
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['trace'],
                    'categories' => ['dev'],
                    // 'logVars' => ['_GET','_POST'],
                    'logFile' => '@app/runtime/logs/trace.log',
                    'maxFileSize' => 1024 * 2,
                    'maxLogFiles' => 50,
                ]
            ],
        ],
    ], $db), 
];
