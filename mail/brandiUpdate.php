<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\DetailView;
use common\widgets\CheckboxWidget;

$this->title = ucwords($searchModel->user->display_name);
$this->params['breadcrumbs'][] = $this->title;
$dataProvider->sort = false;
?>
<div class="recruit-caddy-index">

    <h1><?= Html::encode($this->title) ?></h1>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            // 'filterModel' => $searchModel,
            'emptyCell'=>'',
            'tableOptions' => [
                'class' => 'table table-condensed table-hover table-bordered table-nonfluid col-centered',
            ],
            'columns' => [
                [
                    'class' => 'yii\grid\SerialColumn',
                    'contentOptions'=>['VALIGN'=>'TOP']
                ],
                [
                    'attribute' => 'schoolName',
                    'label'=> 'School',
                    'content' => function ($model, $key, $index, $column){
                        return $model->schoolName.Html::tag('div',Html::tag('div','<b>Likes</b></br>'.$model->likes).Html::tag('div','<b>Dislikes</b></br>'.$model->dislikes),['class'=>'text-center']);
                    }
                ],
                [
                    'attribute' => 'intro_email_sent',
                    'label' => 'Initial Email',
                    'filter' => Html::activeDropDownList($searchModel, 'intro_email_sent', ['0' => 'No', '1' => 'Yes'] ,['class'=>'form-control selectpicker','prompt' => 'All']),
                    'content' => function ($model, $key, $index, $column){
                        return CheckboxWidget::widget([
                            'model'=> $model,
                            'name' => 'intro_email_sent_'.$model->id,
                            'attribute' => 'intro_email_sent',
                            'options'=>[
                                'class'=>'yes-no',
                                'data'=>[
                                    'id'=>$model->id,
                                    'rel'=>'intro_email_sent'
                                ],
                            ]
                        ]);
                    }
                ],
                [
                    'attribute' => 'intro_call_made',
                    'label'=> 'Initial Call',
                    'filter' => Html::activeDropDownList($searchModel, 'intro_call_made', ['0' => 'No', '1' => 'Yes'] ,['class'=>'form-control selectpicker','prompt' => 'All']),
                    'content' => function ($model, $key, $index, $column){
                        return CheckboxWidget::widget([
                            'model'=> $model,
                            'name' => 'intro_call_made'.$model->id,
                            'attribute' => 'intro_call_made',
                            'options'=>[
                                'class'=>'yes-no',
                                'data'=>[
                                    'id'=>$model->id,
                                    'rel'=>'intro_call_made'
                                ],
                            ]
                        ]);
                    }
                ],
                [
                    'attribute' => 'follow_email_sent',
                    'label' => 'Follow Up Email',
                    'filter' => Html::activeDropDownList($searchModel, 'follow_email_sent', ['0' => 'No', '1' => 'Yes'] ,['class'=>'form-control selectpicker','prompt' => 'All']),
                    'content' => function ($model, $key, $index, $column){
                        return CheckboxWidget::widget([
                            'model'=> $model,
                            'name' => 'follow_email_sent'.$model->id,
                            'attribute' => 'follow_email_sent',
                            'options'=>[
                                'class'=>'yes-no',
                                'data'=>[
                                    'id'=>$model->id,
                                    'rel'=>'follow_email_sent'
                                ],
                            ]
                        ]);
                    }
                ],
                [
                    'attribute' => 'follow_call_made',
                    'label' => 'Follow Up Call',
                    'filter' => Html::activeDropDownList($searchModel, 'follow_call_made', ['0' => 'No', '1' => 'Yes'] ,['class'=>'form-control selectpicker','prompt' => 'All']),
                    'content' => function ($model, $key, $index, $column){
                        return CheckboxWidget::widget([
                            'model'=> $model,
                            'name' => 'follow_call_made'.$model->id,
                            'attribute' => 'follow_call_made',
                            'options'=>[
                                'class'=>'yes-no',
                                'data'=>[
                                    'id'=>$model->id,
                                    'rel'=>'follow_call_made'
                                ],
                            ]
                        ]);
                    }
                ],
                [
                    'attribute' => 'visit',
                    'filter' => Html::activeDropDownList($searchModel, 'visit', ['0' => 'No', '1' => 'Yes'] ,['class'=>'form-control selectpicker','prompt' => 'All']),
                    'content' => function ($model, $key, $index, $column){
                        return CheckboxWidget::widget([
                            'model'=> $model,
                            'name' => 'visit'.$model->id,
                            'attribute' => 'visit',
                            'options'=>[
                                'class'=>'yes-no',
                                'data'=>[
                                    'id'=>$model->id,
                                    'rel'=>'visit'
                                ],
                            ]
                        ]);
                    }
                ],
                [
                    'attribute' => 'offer_received',
                    'label' => 'Offer?',
                    'filter' => Html::activeDropDownList($searchModel, 'offer_received', ['0' => 'No', '1' => 'Yes'] ,['class'=>'form-control selectpicker','prompt' => 'All']),
                    'content' => function ($model, $key, $index, $column){
                        return CheckboxWidget::widget([
                            'model'=> $model,
                            'name' => 'offer_received'.$model->id,
                            'attribute' => 'offer_received',
                            'options'=>[
                                'class'=>'yes-no',
                                'data'=>[
                                    'id'=>$model->id,
                                    'rel'=>'offer_received'
                                ],
                            ]
                        ]);
                    }
                ],
                [
                    'attribute'=>'updated_at',
                    'format' => 'date',
                    'label'=>'Last Updated',
                    'filter'=> false,
                ],
            ],
        ]); ?>
</div>
