<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\DetailView;


$url = (defined('WP_SITEURL'))?WP_SITEURL:get_option( 'siteurl' );
?>
<p>
    You have a new message in Next Steps from <?=$message->display_name?>.
</p>
<p>
    School: <?=$message->schoolName?>
</p>
<p>
    Message: <?=$message->body?>
</p>
<p>
    Log into Brandi Jackson Golf to see full message: <?=Html::a('Log In',$url)?>
</p>