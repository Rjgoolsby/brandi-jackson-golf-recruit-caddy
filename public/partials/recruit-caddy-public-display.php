<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://pyrotechsolutions.com
 * @since      1.0.0
 *
 * @package    Caddy
 * @subpackage Caddy/public/partials
 */
 $url = (defined('WP_SITEURL'))?WP_SITEURL:get_option( 'siteurl' );
?>

<style>
.builder-module-content .builder-module-element {
    overflow-x: scroll!important;
    padding-right: 3%!important;
}
/*#recruit-caddy-container{
    overflow-x: scroll;
    overflow-y: hidden;
}*/
</style>

    <iframe
    id='recruit-caddy-container'
    src="<?=$url?>/wp-content/plugins/recruit-caddy/app/web/recruit-caddy/index"
    style="border:0px #ffffff none;"
    name="myiFrame"
    scrolling="yes"
    frameborder="0"
    height="100%"
    width="100%"
    marginheight="0px"
    marginwidth="0px"
    allowfullscreen
    >
    </iframe>
    <script type="text/javascript">
        jQuery("#recruit-caddy-container").load(function() {
            var src = jQuery('#recruit-caddy-container').attr('src');
            var w = jQuery('#recruit-caddy-container').contents().width();
            var h = jQuery('#recruit-caddy-container').contents().height();
            w+5;
            jQuery(this).height(h+"px");
            jQuery(this).width(w+"px");
            console.log(w);
            console.log(h);
            console.log(src);
        });
    </script>
