
var Template = 
` 
<v-layout row wrap> 
    <v-flex xs12>   
        <v-card> 
            <v-card-text>
                <add-schools :subType="subscription"  :isAdmin="false" :userId="userId"/>
            </v-card-text>
            <v-card-text>
                <caddy-review :subType="subscription"  :isAdmin="false" :userId="userId"/>
            </v-card-text>
        </v-card>
    </v-flex>
</v-layout> 
`  
 
import CaddyReview from '../../shared/components/caddy-review.js';
import AddSchools from '../../shared/components/add-schools.js';

export default { 
    name:'main-app',
    template: Template, 
    components:{
        CaddyReview, 
        AddSchools,
    },
    data() {
        return { 
            userId: undefined,
            subscription:undefined,
            dbRef:undefined,
        }
    },
    created(){
        var vm = this;
        this.userId =   caddyOptions.userId; 
        this.dbRef = firebase.database().ref(`players/${this.userId}`);
        this.dbRef.on('value',(snapshot)=>{  
            console.log(snapshot.val().subscription);
            vm.subscription = snapshot.val().subscription; 
        });
    },
    beforeDestroy(){
        if(this.dbRef != undefined){ 
            this.dbRef.off();
        }
    },
    mounted(){
        var vm = this;
        this.$nextTick(function(){
            console.log('in user-caddy')
            // firebase.database().ref('test').set({
            //     msg:'Hello World 2',
            // });
        })
    },
    computed:{
    },
    methods: {
    }
  }