<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://www.pyrotechsolutions.com
 * @since      1.0.0
 *
 * @package    Recriut_Caddy
 * @subpackage Recriut_Caddy/admin
 */

// $url = (defined('WP_SITEURL'))?WP_SITEURL:get_option( 'siteurl' ); 

$current_user = wp_get_current_user();
// \Yii::trace($current_user,'dev');
$name = (empty($current_user->display_name)) ? $current_user->user_email : $current_user->display_name; 
// $userID = get_current_user_id();
// $userInfo = get_userdata($userID);
// print_r($userInfo);
// echo wp_create_nonce( 'wp_rest' );
?>
<div id="app">
    <v-app> 
        <v-content>
            <v-container fill-height fluid pa-0> 
                <router-view></router-view>
            </v-content>
    </v-app>
</div>
<style>
input[type=checkbox], input[type=color], input[type=date], input[type=datetime-local], input[type=datetime], input[type=email], input[type=month], input[type=number], input[type=password], input[type=radio], input[type=search], input[type=tel], input[type=text], input[type=time], input[type=url], input[type=week], select, textarea {
    border:none;
    box-shadow: none;
}
input.readonly, input[readonly], textarea.readonly, textarea[readonly] {
    background-color: transparent;
}
</style>
<script>
var caddyOptions = {
    isLoggedIn: '<?=is_user_logged_in()?>',
    email:'<?= $current_user->user_email?>',
    nonce: '<?= wp_create_nonce( 'wp_rest' )?>',
    name: '<?=$name?>',
    userId:'<?=$current_user->ID?>'
};  
</script>