<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://pyrotechsolutions.com
 * @since      1.0.0
 *
 * @package    Recruit_Caddy
 * @subpackage Recruit_Caddy/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-specific stylesheet and JavaScript.
 *
 * @package    Recruit_Caddy
 * @subpackage Recruit_Caddy/public
 * @author     Pyrotech Solutions <developers@pyrotechsolutions.com>
 */
class Recruit_Caddy_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	protected $optionsComponents = [];

	protected $mainAppComponents = [
		'recruit-caddy-public-main',
		'recruit-caddy-public-caddy-review'
	];

	protected $defaultComponents = [
		'vuetify-confirm',
		'vuetify',
		'validators'
	];

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		// wp_register_script( 'caddy-app', plugin_dir_url( __FILE__ ) . 'app/Main-App.js', $mainAppComponents, null, true );

	}

	public function set_app_js_mode($tag, $handle, $src ){

		// $query = \app\models\caddy\RecruitCaddy::find()->where(['<>','id','null'])->limit(1)->asArray()->one();
		// \Yii::trace($query,'dev');
		// if(strpos($tag, 'text/javascript') === false){
		// 	$tag = str_replace( '<script', '<script type="text/javascript"', $tag );
		// }
		// \Yii::trace('public ' . $tag,'dev');
		$templates = array_merge_recursive($this->mainAppComponents,$this->optionsComponents,['recruit-caddy-public-app']);
		if (in_array($handle, $templates)){
			$tag = str_replace( 'text/javascript', 'module', $tag );
		}
		// \Yii::trace('public ' . $tag,'dev');
		return $tag;
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Caddy_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Caddy_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		// wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/caddy-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Caddy_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Caddy_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		// wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/caddy-public.js', array( 'jquery','vue' ), $this->version, false );

	}

	public function initMenuItems(){
		add_submenu_page( $parent_slug = null, $page_title = 'Recruit Caddy Users', $menu_title = 'Recruit Caddy Users',  $capability = 'manage_options', $menu_slug = 'recruit-caddy-users',  $function = [$this, 'show_users']);
	}



	protected function registerScripts(){
		$mainAppComponents = array_merge_recursive($this->mainAppComponents,$this->defaultComponents);
		$optionsComponents = array_merge_recursive($this->optionsComponents,$this->defaultComponents);

		//Register App Pages
		wp_register_script( 'recruit-caddy-public-app', plugin_dir_url( __FILE__ ) . 'app/Main-App.js', $mainAppComponents, null, true );

		//COMPONENETS
		wp_register_script( 'recruit-caddy-public-main', plugin_dir_url( __FILE__ ) . 'components/main-app.js', ['vuetify'], null, true );
		wp_register_script( 'recruit-caddy-public-caddy-review', plugin_dir_url(dirname(__FILE__,1)). 'shared/components/caddy-review.js', ['vuetify'], null, true );

	}
	public function recruitCaddyDisplay(){
		$this->registerScripts();
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/recruit-caddy-public.css', ['recruit-caddy-css'], $this->version, 'all' );
		// wp_enqueue_style('caddy-css');
		wp_enqueue_script( 'firebase-bundle');
		wp_enqueue_script( 'recruit-caddy-public-app');
		$this->render();
	}

	public function load_project(){
		add_shortcode('recruit_caddy_player_display',[$this, 'recruitCaddyDisplay']);
	}


	public function render() {
		include('App.php');
	}
}
