<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://pyrotechsolutions.com
 * @since      1.0.0
 *
 * @package    Caddy
 * @subpackage Caddy/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Recruit_Caddy
 * @subpackage Recruit_Caddy/includes
 * @author     Pyrotech Solutions <developers@pyrotechsolutions.com>
 */
class Recruit_Caddy_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */

	public static function deactivate() {
		/* defined('YII_DEBUG') or define('YII_DEBUG', true);
		defined('YII_ENV') or define('YII_ENV', 'dev');

		require(__DIR__ . '/../app/vendor/autoload.php');
		require(__DIR__ . '/../app/vendor/yiisoft/yii2/Yii.php');
		require(__DIR__ . '/../app/common/config/bootstrap.php');
		require(__DIR__ . '/../app/console/config/bootstrap.php');

		$application = yii\helpers\ArrayHelper::merge(
		    require(__DIR__ . '/../app/common/config/main.php'),
		    require(__DIR__ . '/../app/common/config/main-local.php'),
		    require(__DIR__ . '/../app/console/config/main.php'),
		    require(__DIR__ . '/../app/console/config/main-local.php')
		);

		new yii\console\Application($application); // Do NOT call run() here

		\Yii::$app->runAction('migrate/down', ['interactive' => false]);
*/
		$slug = 'recruit-caddy';
		if(($model = self::get_page_by_slug($slug)) !== null){
			// Set the post ID so that we know the post was created successfully
			wp_delete_post($model->ID,true);
		}
	}


	public static function get_page_by_slug($slug) {
	    if ($pages = get_pages()){
	        foreach ($pages as $page){
	            if ($slug === $page->post_name) {
					return $page;
				}
			}
		}
	}
}
