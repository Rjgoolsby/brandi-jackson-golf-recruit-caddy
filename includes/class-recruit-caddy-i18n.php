<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://pyrotechsolutions.com
 * @since      1.0.0
 *
 * @package    Recruit_Caddy
 * @subpackage Recruit_Caddy/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Recruit_Caddy
 * @subpackage Recruit_Caddy/includes
 * @author     Pyrotech Solutions <developers@pyrotechsolutions.com>
 */
class Recruit_Caddy_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'recruit-caddy',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
