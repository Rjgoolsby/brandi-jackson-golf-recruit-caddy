<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://pyrotechsolutions.com
 * @since      1.0.0
 *
 * @package    Recruit_Caddy
 * @subpackage Recruit_Caddy/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Recruit_Caddy
 * @subpackage Recruit_Caddy/includes
 * @author     Pyrotech Solutions <developers@pyrotechsolutions.com>
 */
class Recruit_Caddy_Firebase {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function login($user,$sec) {
        \Yii::trace($user,'dev');
        \Yii::trace($sec,'dev');
    }


    public function afterLogin($user,$sec) {
        \Yii::trace($user,'dev');
        \Yii::trace($sec,'dev');
    }

    // Get your service account's email address and private key from the JSON key file
    // $service_account_email = "abc-123@a-b-c-123.iam.gserviceaccount.com";
    // $private_key = "-----BEGIN PRIVATE KEY-----...";


    /* function create_custom_token($uid, $is_premium_account) {
        $config = \yii\helpers\Json::decode(file_get_contents(\Yii::getAlias('@app/config/firebase-config.json')));

        global $service_account_email, $private_key;

        $now_seconds = time();
        $payload = array(
            "iss" => $config['client_email'],
            "sub" => $config['client_email'],
            "aud" => "https://identitytoolkit.googleapis.com/google.identity.identitytoolkit.v1.IdentityToolkit",
            "iat" => $now_seconds,
            "exp" => $now_seconds+(60*60),  // Maximum expiration time is one hour
            "uid" => $uid,
            "claims" => array(
            "premium_account" => $is_premium_account
            )
        );
        return JWT::encode($payload, $private_key, "RS256");
    } */

}
