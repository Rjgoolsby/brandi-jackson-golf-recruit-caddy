<?php

use app\models\schools\RecruitCaddySchoolsSearch;
use app\models\schools\RecruitCaddySchools;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;
/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://pyrotechsolutions.com
 * @since      1.0.0
 *
 * @package    Caddy
 * @subpackage Caddy/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Recruit_Caddy
 * @subpackage Recruit_Caddy/includes
 * @author     Pyrotech Solutions <developers@pyrotechsolutions.com>
 */
class Recruit_Caddy {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Recruit_Caddy_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_name = 'recruit-caddy';
		$this->version = '2.0.0';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
		// $this->loginFirebase();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Caddy_Loader. Orchestrates the hooks of the plugin.
	 * - Caddy_i18n. Defines internationalization functionality.
	 * - Caddy_Admin. Defines all hooks for the admin area.
	 * - Caddy_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-recruit-caddy-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-recruit-caddy-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-recruit-caddy-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-recruit-caddy-public.php';

		// require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-recruit-caddy-firebase.php';

		$this->loader = new Recruit_Caddy_Loader();


		// Javascript
		// Firebase
		wp_register_script( 'firebase', 'https://www.gstatic.com/firebasejs/6.2.4/firebase-app.js', [], null, false );
		wp_register_script( 'firebase-db', 'https://www.gstatic.com/firebasejs/6.2.4/firebase-database.js', ['firebase'], null, false );
		wp_register_script( 'firebase-auth', 'https://www.gstatic.com/firebasejs/6.2.4/firebase-auth.js', ['firebase'], null, false );
		wp_register_script( 'firebase-bundle', null, ['firebase-auth','firebase-db'], null, false );

		//VUE JS
		wp_register_script( 'axios', 'https://unpkg.com/axios/dist/axios.min.js', [], null, true );
		wp_register_script( 'vue', 'https://unpkg.com/vue@2.6.10/dist/vue.js', ['axios','moment'], null, true );
		wp_register_script( 'vuetify', 'https://cdn.jsdelivr.net/npm/vuetify@1.x/dist/vuetify.js', ['vue','vue-router'],null, true );
		wp_register_script( 'vue-router', 'https://unpkg.com/vue-router/dist/vue-router.js', ['vue'], null, true );
		wp_register_script( 'vuetify-confirm', 'https://cdn.jsdelivr.net/npm/vuetify-confirm@0.2.6/dist/vuetify-confirm.min.js', ['vuetify'], null, true );
		wp_register_script( 'vuelidate', 'https://cdn.jsdelivr.net/npm/vuelidate@0.7.4/dist/vuelidate.min.js', ['vue'], null, true );
		wp_register_script( 'validators', 'https://cdn.jsdelivr.net/npm/vuelidate@0.7.4/dist/validators.min.js', ['vuelidate'], null, true );
		wp_register_script( 'moment', 'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js', [], null, true );

		// CSS
		wp_register_style( 'vuetify-fonts', "https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons", [], null, 'all' );
		wp_register_style( 'vuetify', 'https://cdn.jsdelivr.net/npm/vuetify@1.x/dist/vuetify.min.css', ['vuetify-fonts'], null, 'all' );
		wp_register_style( 'recruit-caddy-css',plugin_dir_url( dirname(__FILE__,1) ) . 'shared/css/recruit-caddy.css', ['vuetify'], null, 'all' );

		$this->loader->add_action( 'rest_api_init', $this, 'querySchools' );
		$this->loader->add_action( 'rest_api_init', $this, 'getAllSchools' );
		$this->loader->add_action( 'rest_api_init', $this, 'getSchoolInformation' );
	}

	// public function loginFirebase(){

	// 	$plugin_firebase = new Caddy_Firebase();

	// 	$this->loader->add_action( 'wp_authenticate', $plugin_firebase, 'login',10,2 );
	// 	$this->loader->add_action( 'wp_login', $plugin_firebase, 'afterLogin',10,2 );
	// }



	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Caddy_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Recruit_Caddy_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	// private function firebaseLogin(){
	// 	$plugin_firebase = new Caddy_Firebase();
	// 	$this->loader->add_action( 'wp_authenticate' ,$plugin_firebase, 'login' );
	// }

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Recruit_Caddy_Admin( $this->get_plugin_name(), $this->get_version() );

		// add_filter( 'script_loader_tag', [$plugin_admin, 'set_app_js_mode'] , 10, 3 );


		$this->loader->add_action( 'rest_api_init', $plugin_admin, 'getLegacyUsers' );
		$this->loader->add_action( 'rest_api_init', $plugin_admin, 'getLegacyUserCaddy' );
		$this->loader->add_action( 'rest_api_init', $plugin_admin, 'getLegacySchools' );
		$this->loader->add_action( 'rest_api_init', $plugin_admin, 'getLegacyMessages' );
		$this->loader->add_action( 'rest_api_init', $plugin_admin, 'getLegacyTransactions' );


		$this->loader->add_filter('script_loader_tag',$plugin_admin,'set_app_js_mode',10,3);
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'initAdminMenuItems' );

	}


	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Recruit_Caddy_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_filter('script_loader_tag',$plugin_public,'set_app_js_mode',10,3);
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		$this->loader->add_action( 'init', $plugin_public, 'load_project' );
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Recruit_Caddy_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}


	public function querySchools(){
		register_rest_route( 'recruit-caddy', '/query-schools', [
			'methods' => 'POST',
			'callback' => function(){
				$searchModel = new RecruitCaddySchoolsSearch();
				return $searchModel->search(\Yii::$app->request->post());
			}
		]);
	}
	public function getAllSchools(){
		register_rest_route( 'recruit-caddy', '/get-all-schools', [
			'methods' => 'GET',
			'callback' => function(){
				return RecruitCaddySchools::find()->where(['gender'=> 20])->asArray()->all();
				// return json_encode([
				// 	'levels' => ArrayHelper::getColumn(RecruitCaddySchools::find()->select('level')->distinct()->asArray()->all(),'level'),
				// 	'schools' =>  RecruitCaddySchools::find()->asArray()->all()
				// ]);
			}
		]);
	}
	public function getSchoolInformation(){
		register_rest_route( 'recruit-caddy', '/get-school-information', [
			'methods' => 'POST',
			'callback' => function(){

				$tid = \Yii::$app->request->post('tid');
				// \Yii::trace(\Yii::$app->request->post(),'dev');
				$client = new Client(['baseUrl' => 'http://new.collegegolf.com']);
				$loginResponse = $client->createRequest()
					->setUrl('login/authenticate.cfm')
					->addHeaders(['content-type' => 'application/x-www-form-urlencoded'])
					->setData([
						'login_type' => 'user',
						'login_userid' => 'brandi@brandijacksongolf.com',
						'login_pwd' => 'bjgolf12'
					])
					->send();
				$cookies['CFID'] = explode(';', $loginResponse->headers->toArray()['set-cookie'][0])[0];
				$cookies['CFTOKEN'] = explode(';', $loginResponse->headers->toArray()['set-cookie'][1])[0];
				return 'http://new.collegegolf.com/ping/school_detail.cfm?tid='.$tid.'&'.$cookies['CFID'].'&'.$cookies['CFTOKEN'];
				// return \Yii::$app->response->redirect('http://new.collegegolf.com/ping/school_detail.cfm?tid='.$tid.'&'.$cookies['CFID'].'&'.$cookies['CFTOKEN']);
			}
		]);
	}

}
