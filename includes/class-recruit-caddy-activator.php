<?php

/**
 * Fired during plugin activation
 *
 * @link       http://pyrotechsolutions.com
 * @since      1.0.0
 *
 * @package    Caddy
 * @subpackage Caddy/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Recruit_Caddy
 * @subpackage Recruit_Caddy/includes
 * @author     Pyrotech Solutions <developers@pyrotechsolutions.com>
 */
class Recruit_Caddy_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		/* defined('YII_DEBUG') or define('YII_DEBUG', true);
		defined('YII_ENV') or define('YII_ENV', 'dev');

		require(__DIR__ . '/../app/vendor/autoload.php');
		require(__DIR__ . '/../app/vendor/yiisoft/yii2/Yii.php');
		require(__DIR__ . '/../app/common/config/bootstrap.php');
		require(__DIR__ . '/../app/console/config/bootstrap.php');

		$application = yii\helpers\ArrayHelper::merge(
		    require(__DIR__ . '/../app/common/config/main.php'),
		    require(__DIR__ . '/../app/common/config/main-local.php'),
		    require(__DIR__ . '/../app/console/config/main.php'),
		    require(__DIR__ . '/../app/console/config/main-local.php')
		);

		new yii\console\Application($application); // Do NOT call run() here

		\Yii::$app->runAction('migrate/up', ['interactive' => false]); */

		// \Yii::$app->runAction('cron/set-schools', ['interactive' => false]);

		$slug = 'recruit-caddy';
		if(self::get_page_by_slug($slug) === null){
			// Set the post ID so that we know the post was created successfully
			$author_id = get_current_user_id();
			$title = 'Recruit Caddy Application';
			$shortCode = '[recruit_caddy_player_display]';
			$post_id = wp_insert_post(
				array(
					'comment_status'	=>	'closed',
					'ping_status'		=>	'closed',
					'post_author'		=>	$author_id,
					'post_name'			=>	$slug,
					'post_title'		=>	$title,
					'post_status'		=>	'publish',
					'post_type'			=>	'page',
					"post_content"		=>  $shortCode
				)
			);
			$model = self::get_page_by_slug($slug);
			update_post_meta($model->ID, '_custom_layout', '51aa16e7e3a0d', $prev_value = '');
		}
	}

	public static function get_page_by_slug($slug) {
	    if ($pages = get_pages()){
	        foreach ($pages as $page){
	            if ($slug === $page->post_name) {
					return $page;
				}
			}
		}
	}
	// add_filter( 'after_setup_theme', 'programmatically_create_post' );
}
