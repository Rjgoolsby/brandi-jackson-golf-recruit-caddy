<?php
use app\models\players\RecruitCaddyPlayers;
use app\models\caddy\RecruitCaddy;
use app\models\schools\RecruitCaddySchools;
use app\models\message\RecruitMessageTable;
use app\models\transactions\RecruitCaddyTransactions;

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://pyrotechsolutions.com
 * @since      1.0.0
 *
 * @package    Caddy
 * @subpackage Caddy/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Caddy
 * @subpackage Caddy/admin
 * @author     Pyrotech Solutions <developers@pyrotechsolutions.com>
 */
class Recruit_Caddy_Admin {
	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	protected $optionsComponents = [
		'recruit-caddy-admin-options',
		'recruit-caddy-admin-legacy-migrations'
	];

	protected $mainAppComponents = [
		'recruit-caddy-admin-main',
		'recruit-caddy-admin-user-caddy',
		'recruit-caddy-admin-caddy-review'
	];

	protected $defaultComponents = [
		'vuetify-confirm',
		'vuetify',
		'validators'
	];

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	public function set_app_js_mode($tag, $handle, $src ){

		// $query = \app\models\caddy\RecruitCaddy::find()->where(['<>','id','null'])->limit(1)->asArray()->one();
		// \Yii::trace($query,'dev');

		if(strpos($tag, 'text/javascript') === false){
			$tag = str_replace( '<script', '<script type="text/javascript"', $tag );
		}
		// \Yii::trace('admin ' . $tag,'dev');
		$templates = array_merge_recursive($this->mainAppComponents,$this->optionsComponents,['recruit-caddy-admin-app','recruit-caddy-admin-options-app']);
		if (in_array($handle, $templates)){
			$tag = str_replace( 'text/javascript', 'module', $tag );
		}
		// \Yii::trace('admin ' . $tag,'dev');
		return $tag;
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
		// wp_enqueue_style( $this->plugin_name, plugin_dir_url( dirname(__FILE__,1) ) . 'shared/css/caddy.css', [], $this->version, 'all' );
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		// wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/caddy-admin.js', array( 'jquery','vuetify'  ), $this->version, false );
	}

	public function getLegacyUsers(){
		register_rest_route( 'recruit-caddy', '/getLegacyUsers', [
			'methods' => 'GET',
			'callback' => function(){
				return RecruitCaddyPlayers::find()->asArray()->all();
			}
		]);
	}

	public function getLegacyUserCaddy(){
		register_rest_route( 'recruit-caddy', '/getLegacyUserCaddy', [
			'methods' => 'GET',
			'callback' => function(){
				return RecruitCaddy::find()->asArray()->all();
			}
		]);
	}
	public function getLegacySchools(){
		register_rest_route( 'recruit-caddy', '/getLegacySchools', [
			'methods' => 'GET',
			'callback' => function(){
				return RecruitCaddySchools::find()->asArray()->all();
			}
		]);
	}

	public function getLegacyMessages(){
		register_rest_route( 'recruit-caddy', '/getLegacyMessages', [
			'methods' => 'GET',
			'callback' => function(){
				return RecruitMessageTable::find()->asArray()->all();
			}
		]);
	}

	public function getLegacyTransactions(){
		register_rest_route( 'recruit-caddy', '/getLegacyTransactions', [
			'methods' => 'GET',
			'callback' => function(){
				return RecruitCaddyTransactions::find()->asArray()->all();
			}
		]);
	}


	public function initAdminMenuItems(){
		add_menu_page( 'Admin Recruit Caddy', 'Recruit Caddy', 'manage_options', 'recruit-caddy', [$this, 'load_project'],'dashicons-networking');
		add_options_page( 'Admin Recruit Caddy Options', 'Recruit Caddy Options', 'manage_options', 'recruit-caddy-options', [$this, 'load_options']);
		add_submenu_page( $parent_slug = null, $page_title = 'Recruit Caddy Users', $menu_title = 'Recruit Caddy Users',  $capability = 'manage_options', $menu_slug = 'recruit-caddy-users',  $function = [$this, 'show_users']);
	}

	protected function registerScripts(){
		$mainAppComponents = array_merge_recursive($this->mainAppComponents,$this->defaultComponents);
		$optionsComponents = array_merge_recursive($this->optionsComponents,$this->defaultComponents);

		//Register App Pages
		wp_register_script( 'recruit-caddy-admin-app', plugin_dir_url( __FILE__ ) . 'app/Main-App.js', $mainAppComponents, null, true );
		wp_register_script( 'recruit-caddy-admin-options-app', plugin_dir_url( __FILE__ ) . 'app/Options-App.js', $optionsComponents, null, true );

		//COMPONENETS
		wp_register_script( 'recruit-caddy-admin-main', plugin_dir_url( __FILE__ ) . 'components/main-app.js', ['vuetify'], null, true );
		wp_register_script( 'recruit-caddy-admin-user-caddy', plugin_dir_url( __FILE__ ) . 'components/user-caddy.js', ['vuetify'], null, true );
		wp_register_script( 'recruit-caddy-admin-options', plugin_dir_url( __FILE__ ) . 'components/options.js', ['vuetify'], null, true );
		wp_register_script( 'recruit-caddy-admin-legacy-migrations', plugin_dir_url( __FILE__ ) . 'components/legacy-migrations.js', ['vuetify'], null, true );
		wp_register_script( 'recruit-caddy-admin-caddy-review', plugin_dir_url(dirname(__FILE__,1)). 'shared/components/caddy-review.js', ['vuetify'], null, true );

	}

	public function load_project(){
		$this->registerScripts();
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/recruit-caddy-admin.css', ['recruit-caddy-css'], $this->version, 'all' );
		wp_enqueue_script( 'firebase-bundle');
		wp_enqueue_script( 'recruit-caddy-admin-app');
		$this->render();
	}

	public function load_options(){
		$this->registerScripts();
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/recruit-caddy-admin.css', ['recruit-caddy-css'], $this->version, 'all' );
		wp_enqueue_script( 'firebase-bundle');
		wp_enqueue_script( 'recruit-caddy-admin-options-app');
		$this->render();
	}

	public function render() {
		include('App.php');
	}
}
