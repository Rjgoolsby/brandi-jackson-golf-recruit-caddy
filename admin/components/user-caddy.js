
var Template = 
` 
<v-layout row wrap> 
    <v-flex xs12>   
        <v-card> 
            <v-toolbar>
                <v-btn to="{ path:'/caddy'}" > <<<< User Dashboard</v-btn>
            </v-toolbar>
            <v-card-text>
            <v-flex xs12>
                <v-select 
                    attach
                    :items="subscriptionTypes"
                    v-model.number="subscription" 
                    label="Subscription"   
                    @change="updateSubscription"
                ></v-select>
            </v-flex>
            </v-card-text>
            <v-card-text>
                <add-schools :subType="subscription" :userId="userId"/>
            </v-card-text>
            <v-card-text>
                <caddy-review :subType="subscription" :userId="userId"/>
            </v-card-text>
        </v-card>
    </v-flex>
</v-layout> 
`  
 
import CaddyReview from '../../shared/components/caddy-review.js';
import AddSchools from '../../shared/components/add-schools.js';

export default { 
    name:'user-caddy',
    template: Template, 
    components:{
        CaddyReview, 
        AddSchools,
    },
    data() {
        return { 
            userId: undefined,
            subscription:undefined,
            dbRef:undefined,
            subscriptionTypes:[
                {
                    text:'Recruit Basic',
                    value:10
                },
                {
                    text:'Elite Premium',
                    value:20,
                }
            ],
        }
    },
    created(){
        var vm = this;
        this.userId =   this.$route.params.userId ; 
        this.dbRef = firebase.database().ref(`players/${this.userId}`);
        this.dbRef.on('value',(snapshot)=>{  
            console.log(snapshot.val().subscription);
            vm.subscription = snapshot.val().subscription; 
        });
    },
    beforeDestroy(){
        if(this.dbRef != undefined){ 
            this.dbRef.off();
        }
    },
    mounted(){
        var vm = this;
        this.$nextTick(function(){
            // firebase.database().ref('test').set({
            //     msg:'Hello World 2',
            // });
        })
    },
    computed:{
    },
    methods: {
        updateSubscription(){
            firebase.database().ref(`players/${this.userId}`).update({
                subscription:this.subscription
            })
        }
    }
  }