
var Template =
`
<v-layout row wrap>
    <v-flex xs12>
        <v-card>
            <v-toolbar>
                <v-toolbar-title> Admin Users </v-toolbar-title>
                <v-spacer/>
                <v-toolbar-items>
                    <v-dialog v-model="dialog" persistent max-width="600px">
                        <template v-slot:activator="{ on }">
                            <v-btn small flat @click="newUserDialog">New Player <v-icon>add_to_photos</v-icon></v-btn>
                        </template>
                        <v-card>
                            <v-card-title>
                                <span class="headline">New User</span>
                            </v-card-title>
                            <v-card-text>
                                <v-container grid-list-md>
                                    <v-layout wrap>
                                        <v-flex xs12>
                                            <v-autocomplete
                                                :items="availableUsers"
                                                label="Available Users"
                                                v-model.number="$v.newUser.id.$model"
                                                :error-messages="newUserIdError"
                                                @input="$v.newUser.id.$touch()"
                                                @blur="$v.newUser.id.$touch()"
                                            />
                                        </v-flex>
                                        <v-flex xs12>
                                            <v-select
                                                :items="subscriptionTypes"
                                                v-model.number="$v.newUser.type.$model"
                                                :error-messages="newUserTypeError"
                                                label="Subscription"
                                                @input="$v.newUser.type.$touch()"
                                                @blur="$v.newUser.type.$touch()"
                                            ></v-select>
                                        </v-flex>
                                    </v-layout>
                                </v-container>
                                <small>*indicates required field</small>
                            </v-card-text>
                            <v-card-actions>
                                <v-spacer></v-spacer>
                                <v-btn color="blue darken-1" flat @click="closeNewUserForm">Close</v-btn>
                                <v-btn color="blue darken-1" flat @click="saveNewUserForm">Save</v-btn>
                            </v-card-actions>
                        </v-card>
                    </v-dialog>
                </v-toolbar-items>
            </v-toolbar>
            <v-card-title>
                Current Users
                <v-spacer></v-spacer>
                <v-text-field
                    v-model="search"
                    append-icon="search"
                    label="Search"
                    single-line
                    hide-details
                    clearable
                ></v-text-field>
            </v-card-title>
            <v-data-table
                :loading="loading"
                :headers="headers"
                :items="users"
                :search="search"
                :rows-per-page-items="rowsPerPageItems"
                :pagination.sync="pagination"
                class="elevation-1">
                <template slot="no-data">
                    <v-alert :value="true" color="error" icon="warning">
                        Sorry, nothing to display here :(
                    </v-alert>
                </template>
                <template slot="items" slot-scope="props">
                    <td  class="hidden-xs-only">#{{ props.item.id }}</td>
                    <td>{{ props.item.name }}</td>
                    <td class="hidden-xs-only">{{ props.item.subType }}</td>
                    <td class="text-xs-center">
                        <v-btn @click="viewUser(props.item.id)" flat small color="green" class="my-0">
                            <v-icon>visibility</v-icon> View
                        </v-btn>
                        <br>
                        <v-btn @click.stop="deleteUser(props.item.id)" flat small color="red">
                            <v-icon>delete_forever</v-icon> Delete
                        </v-btn>
                    </td>
                </template>
            </v-data-table>
        </v-card>
    </v-flex>
</v-layout>
`

Vue.use(window.vuelidate.default)
const { required } = window.validators

export default {
    name: 'main-app',
    template: Template,
    validations: {
        newUser:{
            id:{required},
            type:{required}
        }
    },
    data() {
        return {
            newUser:{
                id:undefined,
                type:10,
            },
            dialog:false,
            members:{},
            users:[],
            loading:false,
            search: '',
            subscriptionTypes:[
                {
                    text:'Recruit Basic',
                    value:10
                },
                {
                    text:'Elite Premium',
                    value:20,
                }
            ],
            headers :[
                {
                    text: 'User Id',
                    align: 'left',
                    sortable: true,
                    value:'id',
                    class: 'hidden-xs-only'
                },
                {
                    text: 'Player',
                    align: 'left',
                    sortable: true,
                    value:'name'
                },
                {
                    text: 'Subscription Type',
                    align: 'left',
                    sortable: true,
                    value:'subType',
                    class: 'hidden-xs-only'

                },
                {
                    text: 'Actions',
                    align: 'center',
                    sortable: false,
                    value:'actions'
                }
            ],
            rowsPerPageItems: [10,20, 40,{"text":"$vuetify.dataIterator.rowsPerPageAll","value":-1}],
            pagination: {
                rowsPerPage: 10
            },
            userDb:{},
            dbRef:undefined,
        }
    },
    created(){
        var vm = this;
        this.getUserDb();
        this.dbRef = firebase.database().ref('players');
        this.dbRef.on('value',(snapshot)=>{
            vm.members = snapshot.val();
            console.log('current members',vm.members);
            vm.setUsers();
        })
    },
    beforeDestroy(){
        if(this.dbRef != undefined){
            this.dbRef.off();
        }
    },
    mounted(){
        var vm = this;
        this.$nextTick(function(){
            // firebase.database().ref('test').set({
            //     msg:'Hello World 2',
            // });]
            console.log('main-app mounted');

        })
    },
    computed:{
        availableUsers(){
            var list = [];
            for (const userId in this.userDb) {
                if (this.userDb.hasOwnProperty(userId)) {
                    if(!this.userIds.includes(userId)){
                        const user =  this.userDb[userId];
                        list.push({
                            text: user.name,
                            value: user.id
                        })
                    }
                }
            }

            console.log('available Users', list);
            return list;
        },
        userIds(){
            return Object.keys(this.members);
        },
        newUserTypeError () {
            const errors = []
            if (!this.$v.newUser.type.$dirty) {
                return errors;
            }
            if (!this.$v.newUser.type.required) {
                errors.push('You Must Select A Subscription Type');
            }
            return errors;
        },
        newUserIdError () {
            const errors = []
            if (!this.$v.newUser.id.$dirty) {
                return errors;
            }
            if (!this.$v.newUser.id.required) {
                errors.push('You Must Select A User To Continue');
            }
            return errors;
        },

    },
    methods:{
        saveNewUserForm(){
            var $v = this.$v;
            var vm = this;
            $v.$touch();
            this.loading = true;
            if(!$v.$error){
                firebase.database().ref(`players/${this.newUser.id}`).set({
                    subscription:parseInt(this.newUser.type)
                }, function(error) {
                    if (error) {
                        // The write failed...
                        console.log('Save Failed',error);
                    } else {

                        vm.$router.push({ path: `/recruit-caddy/${vm.newUser.id}` })
                        // vm.closeNewUserForm();
                    }
                });
                this.loading = false;
            }else{
                console.log('form passed failed');
                this.loading = false;
            }
        },
        closeNewUserForm(){
            this.dialog = false;
            this.resetNewUserForm();
        },
        newUserDialog(){
            this.dialog = true;
            this.resetNewUserForm();
        },
        resetNewUserForm(){
            this.newUser = {
                id:undefined,
                type:10,
            }

            this.$v.$reset();
        },
        viewUser(id){
            console.log('View User', id);
            this.$router.push({ path: `/recruit-caddy/${id}` })
        },
        deleteUser(id){
            console.log('Delete User',id);
            this.$confirm('Do you really want to delete this User?',{
                buttonTrueText: 'Delete User',
                buttonFalseText: 'Keep User',
                buttonFalseColor: 'green',
                buttonTrueColor: 'red',
                property: '$confirm'
            }).then(res => {
                if(res){
                    console.log('removing User => '+id);
                    firebase.database().ref(`players/${id}`).remove();
                    firebase.database().ref(`caddys/${id}`).remove();

                }
            })
        },
        getUserDb(){
            var vm = this;
            var list = {};
            axios.get('/wp-json/wp/v2/users', {
                params: {
                    per_page: 100
                },
                headers: {'X-WP-Nonce': caddyOptions.nonce},
            })
            .then(function (response) {
                console.log(response);
                const data = response.data;
                let length = data.length;
                for (let i = 0; i < length; i++) {
                    const user = data[i];
                    if(!list.hasOwnProperty(user.id)){
                        list[user.id] = {};
                        list[user.id] = user;
                    }
                }
                vm.userDb = list;
            })
            .catch(function (error) {
                console.log(error);
            })
            .then(function () {
                // always executed
            });
        },
        setUsers(){
            var vm = this;
            if(this.userIds.length){
                axios.get('/wp-json/wp/v2/users', {
                    params: {
                        per_page: 100,
                        include: this.userIds
                    },
                    headers: {'X-WP-Nonce': caddyOptions.nonce},
                })
                .then(function (response) {
                    vm.users = [];
                    console.log(response);
                    const data = response.data;
                    let length = data.length;
                    for (let i = 0; i < length; i++) {
                        const user = data[i];
                        var subType = 'Unknown';
                        if(vm.userIds.includes(user.id) > -1 && vm.members[user.id].hasOwnProperty('subscription')){
                            subType = (vm.members[user.id].subscription == 20)? 'Elite Premium' :'Recruit Basic';
                        }
                        vm.users.push({
                            id:user.id,
                            name:user.name,
                            subType: subType
                        })
                    }
                })
                .catch(function (error) {
                    console.log(error);
                })
                .then(function () {
                    // always executed
                });
            }
        }
    }
  }