
var Template = 
` 
<v-layout row wrap> 
    <v-flex xs12>  
        <add-schools></add-schools>
        <caddy-review :userId="userId"/>
    </v-flex>
</v-layout> 
`  

import AddSchools from '../../shared/components/add-schools.js'; 
import CaddyReview from '../../shared/components/caddy-review.js';

export default { 
    name:'user-caddy',
    template: Template, 
    components:{
        CaddyReview,AddSchools
    },
    data() {
        return { 
            userId: undefined,
        }
    },
    created(){
        this.userId =   this.$route.params.userId ; 
    },
    mounted(){
        var vm = this;
        this.$nextTick(function(){
            // firebase.database().ref('test').set({
            //     msg:'Hello World 2',
            // });
        })
    },
    computed:{
    },
    methods: {
    }
  }