
var Template =
`
<v-card>
    <v-card-text>
        <v-layout row wrap>
            <v-flex xs12>
                <h1>Legacy Data Migrations</h1>
                <v-divider></v-divider>
            </v-flex>
            <v-flex xs12>
                <h3 class="my-0">Users Records: {{ legacyUsers.length }}</h3>
                <h3 class="my-0">Caddy Records: {{ legacyCaddy.length }}</h3>
                <h3 class="my-0">Messages Records: {{ legacyMessages.length }}</h3>
                <h3 class="my-0">Transactions Records: {{ legacyTransactions.length }}</h3>
                <v-divider></v-divider>
            </v-flex>
            <v-flex xs12>
                <v-alert  :value="true" color="error" icon="warning" outline>
                    Migrating Data will Not Delete Data but it WILL replace any data deleted after 7/4/19
                </v-alert>
                <v-btn block :loading="migrateRunning" color="primary" @click="migrateData">Migrate Data</v-btn>
                <div v-show="migrateRunning">
                    <label>Users Migration {{ userMigrateBuffer }}</label><v-divider></v-divider>
                    <v-progress-linear v-model="userMigrateBuffer"></v-progress-linear>
                    <label>Caddy Migration {{ caddyMigrateBuffer }}</label><v-divider></v-divider>
                    <v-progress-linear v-model="caddyMigrateBuffer"></v-progress-linear>
                    <label>Messages Migration {{ messagesMigrateBuffer }}</label><v-divider></v-divider>
                    <v-progress-linear v-model="messagesMigrateBuffer"></v-progress-linear>
                    <label>Transactions Migration {{ transactionsMigrateBuffer }}</label><v-divider></v-divider>
                    <v-progress-linear v-model="transactionsMigrateBuffer"></v-progress-linear>
                </div>
            </v-flex>
        </v-layout>
    </v-card-text>
</v-card>
`
// import axios from 'axios';
// const axios = require('axios');


export default {
    name:'legacy-migrations',
    template: Template,
    data() {
        return {
            userMigration:false,
            userMigrateBuffer:0,
            caddyMigration:false,
            caddyMigrateBuffer:0,
            messagesMigration:false,
            messagesMigrateBuffer:0,
            transactionsMigration:false,
            transactionsMigrateBuffer:0,
            message: 'Oh hai from the component',
            legacyUsers:[],
            legacyCaddy:[],
            legacyMessages:[],
            legacyTransactions:[],
        }
    },
    mounted(){
        var vm = this;
        this.$nextTick(function(){
            vm.getLegacyUsers();
            vm.getLegacyUserCaddy();
            // vm.getLegacyMessages();
            // vm.getLegacyTransactions();
        })
    },
    computed:{
        migrateRunning(){
            return this.userMigration || this.caddyMigration  || this.messagesMigration || this.transactionsMigration;
        },
    },
    methods: {
        getLegacyUsers(){
            var vm = this;
            jQuery.ajax( {
                url: '/wp-json/recruit-caddy/getLegacyUsers',
                method: 'GET',
                beforeSend: function ( xhr ) {
                    xhr.setRequestHeader( 'X-WP-Nonce', caddyOptions.nonce);
                },
            } ).done( function ( response ) {
                console.log( 'CADDY Users',response );
                vm.legacyUsers = response;;
            } );
        },
        getLegacyUserCaddy(){
            var vm = this;
            jQuery.ajax( {
                url: '/wp-json/recruit-caddy/getLegacyUserCaddy',
                method: 'GET',
                beforeSend: function ( xhr ) {
                    xhr.setRequestHeader( 'X-WP-Nonce', caddyOptions.nonce);
                },
            } ).done( function ( response ) {
                console.log( 'CADDY Caddy',response );
                vm.legacyCaddy = response;
            } );
        },
        getLegacyMessages(){
            var vm = this;
            jQuery.ajax( {
                url: '/wp-json/recruit-caddy/getLegacyMessages',
                method: 'GET',
                beforeSend: function ( xhr ) {
                    xhr.setRequestHeader( 'X-WP-Nonce', caddyOptions.nonce);
                },
            } ).done( function ( response ) {
                // console.log( 'CADDY Messages',response );
                vm.legacyMessages = response;
            } );
        },
        getLegacyTransactions(){
            var vm = this;
            jQuery.ajax( {
                url: '/wp-json/recruit-caddy/getLegacyTransactions',
                method: 'GET',
                beforeSend: function ( xhr ) {
                    xhr.setRequestHeader( 'X-WP-Nonce', caddyOptions.nonce);
                },
            } ).done( function ( response ) {
                // console.log( 'CADDY Transactions',response );
                vm.legacyTransactions = response;
            } );
        },
        migrateData(){
            var vm = this;
            console.log('migrating data');
            this.migrateUsers();
            this.migrateLegacyCaddy();
            // this.migrateLegacyMessages();
            // this.migrateLegacyTransactions();
        },
        async migrateLegacyTransactions(){
            this.transactionsMigration = true;
            this.transactionsMigrateBuffer = 0;
            var dataset = {};
            var length  = this.legacyTransactions.length;
            if(length){
                console.log('Migrating Transactions ' , this.legacyTransactions)
                for (let i = 0; i < length; i++) {
                    const el = this.clone(this.legacyTransactions[i]);
                    const key = el.id;
                    delete el.id;
                    dataset[key] = {};
                    dataset[key] = el;
                    this.transactionsMigrateBuffer = Math.floor((i/length)*100);
                    // console.log(i,length,this.transactionsMigrateBuffer);
                }
            }
            this.transactionsMigrateBuffer = 100;
            console.log('transactions dataset',dataset);
            await firebase.database().ref('transactions').set(dataset);
            this.transactionsMigration = false;
        },
        async migrateLegacyMessages(){
            this.messagesMigration = true;
            this.messagesMigrateBuffer = 0;
            var dataset = {};
            var length  = this.legacyMessages.length;
            if(length){
                // console.log('Migrating Messages ' , this.legacyMessages)
                for (let i = 0; i < length; i++) {
                    const el =this.clone(this.legacyMessages[i]);
                    // const key = el.player_id;
                    // delete el.id;
                    // delete el.player_id;
                    // if(!dataset.hasOwnProperty(key)){
                    //     dataset[key] = [];
                    // }
                    // dataset[key].push(el);
                    this.messagesMigrateBuffer = Math.floor((i/length)*100);
                }
            }
            this.messagesMigrateBuffer = 100;
            // console.log('dataset',dataset);
            // await firebase.database().ref('messages').set(dataset);
            this.messagesMigration = false;
        },
        async migrateLegacyCaddy(){
            this.caddyMigration = true;
            this.caddyMigrateBuffer = 0;
            var dataset = {};
            var length  = this.legacyCaddy.length;
            if(length){
                var users = {};
                var userLength  = this.legacyUsers.length;
                for (let j = 0; j < userLength; j++) {
                    const player = this.clone(this.legacyUsers[j]);
                    if(!users.hasOwnProperty(player.id)){
                        users[player.id] =  parseInt(player.user_id);
                    }
                    // users[player.id] =  parseInt(player.user_id);
                }

                // console.log('Migrating caddys ' , this.legacyCaddy)
                for (let i = 0; i < length; i++) {
                    const el = this.clone(this.legacyCaddy[i]);
                    const key = users[el.player_id];
                    console.log('Searching for User ID ' + el.player_id)
                    if(key != undefined){

                        console.log('player id ' + key)
                        delete el.id;
                        delete el.player_id;
                        delete el.deleted_by;
                        for (const prop in el) {
                            // console.log(prop);
                            if(prop != 'likes' && prop != 'dislikes'){
                                if (el.hasOwnProperty(prop)) {
                                    el[prop] = parseInt(el[prop]);
                                    // console.log(el[prop]);
                                }
                            }
                        }
                        if(!dataset.hasOwnProperty(key)){
                            dataset[key] = {};
                        }
                        //Reformat Element
                        dataset[key][el.school_id] = el;
                    }
                    this.caddyMigrateBuffer = Math.floor((i/length)*100);
                }
            }
            this.caddyMigrateBuffer = 100;
            console.log('migrateLegacyCaddy',dataset);
            await firebase.database().ref('caddys').set(dataset);
            this.caddyMigration = false;
        },
        async migrateUsers(){
            this.userMigration = true;
            this.userMigrateBuffer = 0;
            var dataset = {};
            var userLength  = this.legacyUsers.length;
            if(userLength){
                // console.log('Migrating Users ' , this.legacyUsers)
                for (let i = 0; i < userLength; i++) {
                    const player = this.clone(this.legacyUsers[i]);
                    dataset[player.user_id] = {};
                    dataset[player.user_id] = {
                        subscription: parseInt(player.type)
                    }
                    this.userMigrateBuffer = Math.floor((i/userLength)*100);
                }
            }
            this.userMigrateBuffer = 100;

            console.log('migrateUsers',dataset);
            await firebase.database().ref('players').set(dataset);
            this.userMigration = false;
        },
        clone(obj) {
            return JSON.parse(JSON.stringify(obj));
        },
    }
  }