
// Your web app's Firebase configuration

import mainApp from '../components/main-app.js';
import userCaddy from '../components/user-caddy.js';
import {firebaseLogin, firebaseConfig } from '../../config/utils.js';


Vue.use(VueRouter)

let router = new VueRouter({
    routes: [
        {
            path: '/index',
            name: 'Index',
            component: mainApp
        },
        { path: '*', redirect: '/index' },
        {
            path:'/recruit-caddy/:userId',
            name:'user-caddy',
            component:userCaddy
        }
    ]
})
console.log('initializing firebase');
firebase.initializeApp(firebaseConfig);
firebase.auth().onAuthStateChanged(async function(user){
    var email = caddyOptions.email;
    if(user){
        var app = new Vue({ router }).$mount('#app');
    }else if(!user && email){
        await firebaseLogin(email);
    }
});

